/*
** thread.c for raytracer in /home/gouy_e/raytracer/sources
**
** Made by gouy_e
** Login   <gouy_e@epitech.net>
**
** Started on  Tue Jun  3 14:37:52 2014 gouy_e
** Last update Thu Jun  5 18:16:36 2014 gouy_e
*/

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include "struct_general.h"
#include "raytracer.h"
#include "my.h"

int			launch_thread(t_general *g,
				      pthread_t *core,
				      t_general *g_thread)
{
  int			i;

  i = -1;
  while (++i < g->thread_nbr)
    {
      g->image.x = i;
      g_thread[i] = *g;
      if (pthread_create(&core[i], NULL, ray_cast, &(g_thread[i])) != 0)
	return (1);
    }
  i = -1;
  while (++i < g->thread_nbr)
    if (pthread_join(core[i], NULL) != 0)
      return (1);
  return (0);
}

int			thread_on_ray(t_general *g)
{
  pthread_t		*core;
  t_general		*g_thread;

  if ((core = malloc(sizeof(pthread_t) * g->thread_nbr)) == NULL ||
      (g_thread = malloc(sizeof(t_general) * g->thread_nbr)) == NULL)
    {
      free(core);
      return (1);
    }
  if (launch_thread(g, core, g_thread))
    return (1);
  free(g_thread);
  free(core);
  my_printf("\n");
  return (0);
}
