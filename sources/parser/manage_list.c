/*
** manage_list.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 20:49:48 2014
** Last update Fri Jun  6 19:02:05 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"

int		init_obj_list(t_obj *elem)
{
  elem->refrac = 1.;
  elem->radius = 0.;
  elem->color.color = 0xFFFFFF;
  elem->color2.color = 0xFFFFFF;
  elem->shine = 0.;
  elem->opacity = 0.;
  elem->indice = 1.;
  elem->reflection = 0.;
  elem->specular_radius = 1.;
  elem->specular_intensity = 1.;
  elem->texture = NONE_T;
  elem->imgurl = NULL;
  return (0);
}

int		my_put_object_in_list(t_obj **lexem, t_typeobj type)
{
  t_obj		*elem;
  t_coord	coord;

  coord.x = 0.;
  coord.y = 0.;
  coord.z = 0.;
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  elem->coord = coord;
  elem->rotate = coord;
  elem->limit.start = coord;
  elem->limit.end = coord;
  elem->limit.startuse[0] = 0;
  elem->limit.startuse[1] = 0;
  elem->limit.startuse[2] = 0;
  elem->limit.enduse[0] = 0;
  elem->limit.enduse[1] = 0;
  elem->limit.enduse[2] = 0;
  elem->type = type;
  init_obj_list(elem);
  elem->next = *lexem;
  *lexem = elem;
  return (0);
}

int		my_put_in_list(t_lexem **lexem, char *name,
			       int closed[2], char *parent)
{
  t_lexem	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  elem->name = name;
  elem->closed = closed[0];
  elem->line = closed[1];
  elem->params = NULL;
  elem->value = NULL;
  elem->parent = parent;
  elem->next = *lexem;
  elem->prev = NULL;
  if (*lexem)
    (*lexem)->prev = elem;
  *lexem = elem;
  return (0);
}

int			my_put_params_in_list(t_lexem_params **lexem,
					      char *name,
					      char *value)
{
  t_lexem_params	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  elem->name = name;
  elem->value = value;
  elem->next = *lexem;
  *lexem = elem;
  return (0);
}
