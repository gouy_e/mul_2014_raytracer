/*
** parse.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:42:58 2014
** Last update Tue Jun  3 18:06:21 2014 gouy_e
*/

#include <stdlib.h>
#include <pthread.h>
#include "functions_parse.h"
#include "struct_general.h"
#include "struct_lexem.h"
#include "manage_list.h"
#include "lexem_get.h"
#include "config.h"
#include "error.h"
#include "my.h"

int	get_token(char *line, int nbr_line)
{
  int	i;
  int	double_escape;
  int	simple_escape;

  i = -1;
  double_escape = 0;
  simple_escape = 0;
  while (line[++i])
    {
      if (line[i] == '"')
	double_escape ^= 1;
      if (line[i] == '\'')
	simple_escape ^= 1;
      if (!double_escape && !simple_escape && line[i] == '>')
	return (i - 1);
    }
  print_error("Syntax Error : Invalid configuration's file\n", 'e', nbr_line);
  return (-1);
}

char	*get_token_name(char *token, int s, int nbr_line)
{
  char	*tmp;
  int	i;
  int	j;

  i = -1;
  while (++i < s && is_valid_char(token[i], CHAR_AUT));
  if (i == 0 || (token[i] != ' ' && token[i] != '>'))
    {
      print_error("Syntax Error : Incorrect XML tag in the file\n",
		  'e', nbr_line);
      return (NULL);
    }
  if ((tmp = malloc(sizeof(char) * (i + 1))) == NULL)
    return (NULL);
  j = -1;
  while (++j < i)
    tmp[j] = token[j];
  tmp[j] = 0;
  return (tmp);
}

char	*get_name_of_params(char *token, int s)
{
  char	*tmp;
  int	i;

  if ((tmp = malloc(sizeof(char) * (s + 1))) == NULL)
    return (NULL);
  i = -1;
  while (++i < s)
    tmp[i] = token[i];
  tmp[s] = 0;
  return (tmp);
}

int	get_token_params(t_lexem **lexem, char *token, int s, int nbr_line)
{
  char	*name;
  char	*value;
  int	i;
  int	save;

  i = -1;
  if (s <= 0)
    return (0);
  while (++i < s && is_valid_char(token[i], CHAR_AUT));
  if (i == s || token[i] != '=' || (i + 1) == s)
    return (print_error("Error : Invalid param's Syntax\n", 'e', nbr_line));
  if ((name = get_name_of_params(token, i)) == NULL)
    return (1);
  save = i + 1;
  while (++i < s && is_valid_char(token[i], CHAR_VALUE));
  if (token[i] != ' ' && i < s)
    return (print_error("Error : Invalid param's Syntax\n", 'e', nbr_line));
  if ((value = get_name_of_params(&token[save], i - save)) == NULL)
    return (1);
  if ((value = check_params_value(value, nbr_line)) == NULL)
    return (1);
  if (*lexem)
    if (my_put_params_in_list(&((*lexem)->params), name, value))
      return (1);
  return (get_token_params(lexem, &token[i + 1], s - i - 1, nbr_line));
}

int	my_set_data(char *data, int *i, t_lexem **lexem, int nbr_line)
{
  int	j;
  char	*tmp;

  j = -1;
  while (data[++j] && data[j] != '<');
  if ((tmp = malloc(sizeof(char) * (j + 1))) == NULL)
    return (1);
  *i = -1;
  while (++(*i) < j)
    tmp[*i] = data[*i];
  tmp[*i] = 0;
  if (*lexem)
    {
      if (!((*lexem)->value))
	(*lexem)->value = tmp;
      else
	if (((*lexem)->value = concaten_strs((*lexem)->value, tmp)) == NULL)
	  return (1);
    }
  else
    {
      print_error("Warning : Data without any token\n", 'e', nbr_line);
      free(tmp);
    }
  return (0);
}
