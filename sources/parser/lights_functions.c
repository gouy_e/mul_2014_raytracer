/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 15:24:51 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			light_intensity(t_lexem **tmp, t_light *light)
{
  t_lexem		*token;
  double		value;

  token = (*tmp);
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the color.\n",
		  'w', token->line);
      value = 0xFFFFFF;
    }
  light->intensity = value;
  *tmp = token->prev;
  return (0);
}

int			light_color(t_lexem **tmp, t_light *light)
{
  t_lexem		*token;
  double		value;

  token = (*tmp);
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the color.\n",
		  'w', token->line);
      value = 0xFFFFFF;
    }
  light->color.color = value;
  *tmp = token->prev;
  return (0);
}

int			light_rotation(t_lexem **tmp, t_light *light)
{
  t_lexem		*token;
  double		value;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "rotation"))
	{
	  *tmp = token;
	  return (0);
	}
      value = (token->value) ? atof(token->value) : 0;
      if (!my_strcmp(token->name, "x"))
	light->rotate.x = value;
      else if (!my_strcmp(token->name, "y"))
	light->rotate.y = value;
      else if (!my_strcmp(token->name, "z"))
	light->rotate.z = value;
      else
	print_xml_error(token->name, token->line);
      token = token->prev;
    }
  return (0);
}

int			light_position(t_lexem **tmp, t_light *light)
{
  t_lexem		*token;
  double		value;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "position"))
	{
	  *tmp = token;
	  return (0);
	}
      value = (token->value) ? atof(token->value) : 0;
      if (!my_strcmp(token->name, "x"))
	light->coord.x = value;
      else if (!my_strcmp(token->name, "y"))
	light->coord.y = value;
      else if (!my_strcmp(token->name, "z"))
	light->coord.z = value;
      else
	print_xml_error(token->name, token->line);
      token = token->prev;
    }
  return (0);
}
