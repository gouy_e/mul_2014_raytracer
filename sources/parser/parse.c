/*
** parse.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:42:58 2014
** Last update Tue Jun  3 20:28:47 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "get_next_line.h"
#include "struct_general.h"
#include "struct_lexem.h"
#include "lexem.h"
#include "parsing.h"
#include "error.h"
#include "my.h"

int	verify_lexem(t_lexem *lexem)
{
  while (lexem)
    {
      if (!lexem->closed)
	{
	  if (print_error("Syntax Error : Tags are not closed\n", 'e',
			  lexem->line) == -1)
	    return (1);
	  return (1);
	}
      lexem = lexem->next;
    }
  return (0);
}

void			free_linked_list(t_lexem *lexem)
{
  t_lexem		*tmp;
  t_lexem_params	*params;
  t_lexem		*tmp2;
  t_lexem_params	*params2;

  tmp = lexem;
  while (tmp)
    {
      params = tmp->params;
      while (params)
	{
	  free(params->name);
	  free(params->value);
	  params2 = params;
	  params = params->next;
	  free(params2);
	}
      free(tmp->name);
      if (tmp->value)
	free(tmp->value);
      tmp2 = tmp;
      tmp = tmp->next;
      free(tmp2);
    }
}

int		parse_file(const int fd, t_general *g)
{
  t_lexem	*lexem;
  char		*line;
  int		nbr_line;

  lexem = NULL;
  g->objs = NULL;
  g->cams = NULL;
  g->lights = NULL;
  g->thread_nbr = 1;
  nbr_line = 0;
  while ((line = get_next_line_file(fd)) != NULL)
    {
      ++nbr_line;
      if (check_line(line, &lexem, nbr_line))
	return (1);
      free(line);
  }
  if (verify_lexem(lexem))
    return (1);
  if (my_parsing(g, lexem))
    return (1);
  print_success("File Successfully parsed.\n");
  free_linked_list(lexem);
  return (0);
}
