/*
** parser.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 13:49:17 2014
** Last update Fri Jun  6 19:10:40 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "parse_functions.h"
#include "init_func_tab.h"
#include "error.h"
#include "my.h"

int		my_parsing(t_general *g, t_lexem *lexem)
{
  t_parsing	parser;
  t_lexem	*tmp;
  int		i;

  tmp = lexem;
  while (tmp && tmp->next)
    tmp = tmp->next;
  init_func_tab(&parser);
  while (tmp)
    {
      i = -1;
      while (++i < 4)
	if (!my_strcmp(tmp->name, parser.parser[i].name))
	  {
	    if (parser.parser[i].ptr(g, &tmp, &parser))
	      return (1);
	    i = 3;
	  }
	else if (i == 3)
	  {
	    print_xml_error(tmp->name, tmp->line);
	    tmp = tmp->prev;
	  }
    }
  return (0);
}
