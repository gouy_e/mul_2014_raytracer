/*
** parse.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:42:58 2014
** Last update Fri Jun  6 19:04:31 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "functions_parse.h"
#include "struct_general.h"
#include "struct_lexem.h"
#include "manage_list.h"
#include "lexem_check.h"
#include "config.h"
#include "error.h"
#include "my.h"

int		check_close(char *token, int s, t_lexem **lexem, int nbr_line)
{
  t_lexem	*tmp;
  char		*name;

  if ((name = get_token_name(&token[1], s, nbr_line)) == NULL)
    return (1);
  tmp = *lexem;
  while (tmp && tmp->closed)
    tmp = tmp->next;
  if (my_strcmp(name, tmp->name) == 0)
    {
      tmp->closed = 1;
      free(name);
      return (0);
    }
  else
    {
      free(name);
      print_error("Error : incorrect Syntax in the file\n", 'e', nbr_line);
      return (1);
    }
  return (0);
}

char	*check_params_value(char *value, int nbr_line)
{
  char	*tmp;
  int	i;

  i = -1;
  if (value[0] == '"')
    {
      if (value[my_strlen(value) - 1] != '"')
	{
	  print_error("Error : Params is invalid\n", 'e', nbr_line);
	  return (NULL);
	}
      if ((tmp = get_name_of_params(&value[1], my_strlen(value) - 2)) == NULL)
	return (NULL);
      free(value);
      value = tmp;
    }
  else
    while (value[++i])
      if (!is_valid_char(value[i], CHAR_NUMBER))
	{
	  print_error("Error : Params is invalid (Not a number)\n",
		      'e', nbr_line);
	  return (NULL);
	}
  return (value);
}

int		check_line(char *line, t_lexem **lexem, int nbr_line)
{
  my_epurstr(line);
  if (detect_token(line, lexem, nbr_line))
    return (1);
  return (0);
}
