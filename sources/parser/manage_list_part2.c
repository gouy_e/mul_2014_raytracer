/*
** manage_list.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 20:49:48 2014
** Last update Fri Jun  6 19:01:53 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"

int		my_put_camera_in_list(t_cam **lexem)
{
  t_cam		*elem;
  t_coord	coord;

  coord.x = 0.;
  coord.y = 0.;
  coord.z = 0.;
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  elem->coord = coord;
  elem->rotate = coord;
  elem->next = *lexem;
  *lexem = elem;
  return (0);
}

int		my_put_light_in_list(t_light **lexem, t_typelight type)
{
  t_light	*elem;
  t_coord	coord;

  coord.x = 0.;
  coord.y = 0.;
  coord.z = 0.;
  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (1);
  elem->coord = coord;
  elem->rotate = coord;
  elem->type = type;
  elem->intensity = 1.;
  elem->color.color = 0xFFFFFF;
  elem->next = *lexem;
  *lexem = elem;
  return (0);
}
