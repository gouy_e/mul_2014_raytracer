/*
** parser.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 13:49:17 2014
** Last update Fri Jun  6 19:14:49 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "parse_functions.h"
#include "init_func_tab.h"
#include "error.h"
#include "my.h"

void	init_func_tab_cameras(t_parsing *parser)
{
  parser->cameras[0].name = "position";
  parser->cameras[0].ptr = camera_position;
  parser->cameras[1].name = "rotation";
  parser->cameras[1].ptr = camera_rotation;
  parser->cameras[2].name = NULL;
}

void	init_func_tab_light_type(t_parsing *parser)
{
  parser->ligtype[0].name = "spot";
  parser->ligtype[0].type = SPOT;
  parser->ligtype[1].name = "diffuse";
  parser->ligtype[1].type = DIFFUSE;
  parser->ligtype[2].name = NULL;
}

void	init_func_tab_raytracer(t_parsing *parser)
{
  parser->raytracer[0].name = "screenx";
  parser->raytracer[0].ptr = raytracer_screenx;
  parser->raytracer[1].name = "screeny";
  parser->raytracer[1].ptr = raytracer_screeny;
  parser->raytracer[2].name = "thread";
  parser->raytracer[2].ptr = raytracer_thread;
  parser->raytracer[3].name = "nbray";
  parser->raytracer[3].ptr = raytracer_nbray;
  parser->raytracer[4].name = "export";
  parser->raytracer[4].ptr = raytracer_export;
  parser->raytracer[5].name = NULL;
}

void	init_func_tab(t_parsing *parser)
{
  parser->parser[0].name = "raytracer";
  parser->parser[0].ptr = parse_raytracer;
  parser->parser[1].name = "camera";
  parser->parser[1].ptr = parse_camera;
  parser->parser[2].name = "object";
  parser->parser[2].ptr = parse_object;
  parser->parser[3].name = "light";
  parser->parser[3].ptr = parse_light;
  parser->parser[4].name = NULL;
  init_func_tab_raytracer(parser);
  init_func_tab_object_type(parser);
  init_func_tab_objects(parser);
  init_func_tab_light_type(parser);
  init_func_tab_lights(parser);
  init_func_tab_cameras(parser);
}
