/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 19:25:20 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			object_rotation(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  double		value;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "rotation"))
	{
	  *tmp = token;
	  return (0);
	}
      value = (token->value) ? atof(token->value) : 0;
      if (!my_strcmp(token->name, "x"))
	obj->rotate.x = value;
      else if (!my_strcmp(token->name, "y"))
	obj->rotate.y = value;
      else if (!my_strcmp(token->name, "z"))
	obj->rotate.z = value;
      else
	print_xml_error(token->name, token->line);
      token = token->prev;
    }
  return (0);
}

int			object_position(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  double		value;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "position"))
	{
	  *tmp = token;
	  return (0);
	}
      value = (token->value) ? atof(token->value) : 0;
      if (!my_strcmp(token->name, "x"))
	obj->coord.x = value;
      else if (!my_strcmp(token->name, "y"))
	obj->coord.y = value;
      else if (!my_strcmp(token->name, "z"))
	obj->coord.z = value;
      else
	print_xml_error(token->name, token->line);
      token = token->prev;
    }
  return (0);
}

int			set_obj_limit(t_lexem *token, t_coord *lim, char *use)
{
  double		value;

  value = (token->value) ? atof(token->value) : 0;
  if (!my_strcmp(token->name, "x"))
    {
      use[0] = 1;
      lim->x = value;
    }
  else if (!my_strcmp(token->name, "y"))
    {
      use[1] = 1;
      lim->y = value;
    }
  else if (!my_strcmp(token->name, "z"))
    {
      use[2] = 1;
      lim->z = value;
    }
  else
    print_xml_error(token->name, token->line);
  return (0);
}

int			object_limitmin(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "limitmin"))
	{
	  *tmp = token;
	  return (0);
	}
      set_obj_limit(token, &(obj->limit.start), &(obj->limit.startuse)[0]);
      token = token->prev;
    }
  return (0);
}

int			object_limitmax(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "limitmax"))
	{
	  *tmp = token;
	  return (0);
	}
      set_obj_limit(token, &(obj->limit.end), &(obj->limit.enduse)[0]);
      token = token->prev;
    }
  return (0);
}
