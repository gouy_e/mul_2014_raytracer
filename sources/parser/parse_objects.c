/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 15:27:13 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

t_typeobj		get_type_obj(t_lexem_params *params,
				     t_parsing *prs, int nbr_line)
{
  t_typeobj		type;
  int			i;

  type = SPHERE;
  while (params && my_strcmp(params->name, "type"))
    params = params->next;
  i = -1;
  if (params)
    while (prs->objtype[++i].name)
      if (params->value && !my_strcmp(prs->objtype[i].name, params->value))
	return (prs->objtype[i].type);
  print_error("Warning : The object's type is not initialized\n",
	      'w', nbr_line);
  return (type);
}

int			load_obj_func(t_general *g,
				      t_lexem **token,
				      t_parsing *prs,
				      int *check)
{
  int			i;

  i = -1;
  *check = 0;
  while (prs->objects[++i].name)
    if (!my_strcmp(prs->objects[i].name, (*token)->name))
      {
	if (prs->objects[i].ptr(token, g->objs))
	  return (1);
	++(*check);
	i = 12;
      }
  return (0);
}

int			parse_object(t_general *g,
				     t_lexem **tmp,
				     t_parsing *prs)
{
  int			check;
  t_lexem		*token;
  t_typeobj		type;

  token = (*tmp)->prev;
  type = get_type_obj((*tmp)->params, prs, token->line);
  if (my_put_object_in_list(&(g->objs), type))
    return (1);
  while (token)
    {
      if (my_strcmp(token->parent, "object"))
  	{
  	  *tmp = token;
  	  return (0);
  	}
      if (load_obj_func(g, &token, prs, &check))
	return (1);
      if (!check)
  	{
  	  print_xml_error(token->name, token->line);
  	  token = token->prev;
  	}
    }
  *tmp = token;
  return (0);
}
