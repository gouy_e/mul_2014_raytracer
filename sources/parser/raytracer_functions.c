/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 19:20:10 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			raytracer_screenx(t_general *g, char *value, int line)
{
  if (value)
    g->win.screenx = atof(value);
  else
    {
      print_error("Screenx's value is not set.\n", 'w', line);
      return (0);
    }
  if (g->win.screenx < 10)
    {
      print_error("Screenx's value is too small\n", 'e', line);
      return (1);
    }
  return (0);
}

int			raytracer_screeny(t_general *g, char *value, int line)
{
  if (value)
    g->win.screeny = atof(value);
  else
    {
      print_error("Screeny's value is not set.\n", 'w', line);
      return (0);
    }
  if (g->win.screenx < 10)
    {
      print_error("Screeny's value is too small\n", 'e', line);
      return (1);
    }
  return (0);
}

int			raytracer_nbray(t_general *g, char *value, int line)
{
  if (value)
    g->max_ray = atof(value);
  else
    {
      print_error("ray number is not set.\n", 'w', line);
      return (0);
    }
  if (g->max_ray < 1)
    {
      print_error("ray number value is too small\n", 'w', line);
      g->max_ray = 1;
      return (0);
    }
  return (0);
}

int			raytracer_thread(t_general *g, char *value, int line)
{
  int			val;

  if (value)
    val = my_getnbr(value);
  else
    {
      print_error("thread's value is not set.\n", 'w', line);
      return (0);
    }
  if (val < 1 || val > 56)
    {
      print_error("Thread number is not valid.\n", 'w', line);
      val = 1;
    }
  g->thread_nbr = val;
  return (0);
}

int			raytracer_export(t_general *g, char *value, int line)
{
  char			*val;

  if (value)
    {
      if ((val = malloc(sizeof(char) * (my_strlen(value) + 1))) == NULL)
	{
	  print_error("Error : malloc failed, no name for exporting.\n",
		      'w', line);
	  return (0);
	}
      val = my_strcpy(val, value);
    }
  else
    {
      print_error("Export's name is not set.\n", 'w', line);
      return (0);
    }
  g->export = val;
  return (0);
}
