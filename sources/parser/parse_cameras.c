/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 15:42:34 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			camera_rotation(t_lexem **tmp, t_cam *cam)
{
  t_lexem		*token;
  double		value;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "rotation"))
	{
	  *tmp = token;
	  return (0);
	}
      value = (token->value) ? atof(token->value) : 0;
      if (!my_strcmp(token->name, "x"))
	cam->rotate.x = value;
      else if (!my_strcmp(token->name, "y"))
	cam->rotate.y = value;
      else if (!my_strcmp(token->name, "z"))
	cam->rotate.z = value;
      else
	print_xml_error(token->name, token->line);
      token = token->prev;
    }
  return (0);
}

int			camera_position(t_lexem **tmp, t_cam *cam)
{
  t_lexem		*token;
  double		value;

  token = (*tmp)->prev;
  while (token)
    {
      if (my_strcmp(token->parent, "position"))
	{
	  *tmp = token;
	  return (0);
	}
      value = (token->value) ? atof(token->value) : 0;
      if (!my_strcmp(token->name, "x"))
	cam->coord.x = value;
      else if (!my_strcmp(token->name, "y"))
	cam->coord.y = value;
      else if (!my_strcmp(token->name, "z"))
	cam->coord.z = value;
      else
	print_xml_error(token->name, token->line);
      token = token->prev;
    }
  return (0);
}

int			load_cam_func(t_general *g,
				      t_lexem **token,
				      t_parsing *prs,
				      int *check)
{
  int			i;

  i = -1;
  *check = 0;
  while (prs->cameras[++i].name)
    if (!my_strcmp(prs->cameras[i].name, (*token)->name))
      {
	if (prs->cameras[i].ptr(token, g->cams))
	  return (1);
	++(*check);
	i = 1;
      }
  return (0);
}

int			parse_camera(t_general *g,
				     t_lexem **tmp,
				     t_parsing *prs)
{
  int			check;
  t_lexem		*token;

  token = (*tmp)->prev;
  if (my_put_camera_in_list(&(g->cams)))
    return (1);
  while (token)
    {
      if (my_strcmp(token->parent, "camera"))
	{
	  *tmp = token;
	  return (0);
	}
      if (load_cam_func(g, &token, prs, &check))
	return (1);
      if (!check)
	{
	  print_xml_error(token->name, token->line);
	  token = token->prev;
	}
    }
  *tmp = token;
  return (0);
}
