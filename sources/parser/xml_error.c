/*
** xml_error.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri May 16 20:22:19 2014
** Last update Sat May 17 19:48:56 2014 
*/

#include <unistd.h>
#include "my.h"

int	my_put_nbr_error(int nb)
{
  char	c;

  if (nb < 0)
    {
      write(2, "-", 1);
      nb = nb * (-1);
    }
  if (nb >= 10)
    my_put_nbr(nb / 10);
  c = nb % 10 + '0';
  write(2, &c, 1);
  return (0);
}

int	print_success(char *str)
{
  if (my_putstr("\033[32m") == -1)
    return (1);
  if (my_putstr(str) == -1)
    return (1);
  if (my_putstr("\033[0m") == -1)
    return (1);
  return (0);
}

int	print_error(char *str, char c, int line)
{
  if (c == 'w')
    {
      if (my_puterror("\033[33m") == -1)
	return (1);
    }
  else
    if (my_puterror("\033[31m") == -1)
      return (1);
  if (my_puterror("Line n°") == -1)
    return (1);
  my_put_nbr_error(line);
  if (my_puterror(" - ") == -1)
    return (1);
  if (my_puterror(str) == -1)
    return (1);
  my_puterror("\033[0m");
  return (1);
}

int	print_xml_error(char *token, int line)
{
  if (my_puterror("\033[33m") == -1)
    return (1);
  if (my_puterror("Line n°") == -1)
    return (1);
  my_put_nbr_error(line);
  if (my_puterror(" - Warning : Unknow token : \"") == -1)
    return (1);
  if (my_puterror(token) == -1)
    return (1);
  if (my_puterror("\" in the xml.\n") == -1)
    return (1);
  my_puterror("\033[0m");
  return (0);
}
