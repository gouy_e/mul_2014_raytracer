/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 19:20:33 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			parse_raytracer(t_general *g,
					t_lexem **tmp,
					t_parsing *prs)
{
  t_lexem_params	*params;
  int			i;

  g->win.screenx = 1920.;
  g->win.screeny = 1080.;
  g->thread_nbr = 1;
  g->max_ray = 10;
  g->export = NULL;
  params = (*tmp)->params;
  while (params)
    {
      i = -1;
      while (prs->raytracer[++i].name)
	{
	  if (!my_strcmp(prs->raytracer[i].name, params->name))
	    if (prs->raytracer[i].ptr(g, params->value, (*tmp)->line))
	      return (1);
	}
      params = params->next;
    }
  (*tmp) = (*tmp)->prev;
  return (0);
}

