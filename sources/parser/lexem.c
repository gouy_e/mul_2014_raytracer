/*
** parse.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:42:58 2014
** Last update Fri Jun  6 19:04:50 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "functions_parse.h"
#include "struct_general.h"
#include "struct_lexem.h"
#include "manage_list.h"
#include "lexem_check.h"
#include "config.h"
#include "error.h"
#include "my.h"

char		*my_get_parents(t_lexem **lexem)
{
  t_lexem	*tmp;

  tmp = *lexem;
  while (tmp && tmp->closed)
    tmp = tmp->next;
  if (tmp)
    return (tmp->name);
  return (NULL);
}

void	verify_token(char *token, int *s, int *closed)
{
  if (token[*s - 1] == '/')
    {
      *closed = 1;
      --(*s);
    }
}

int	parse_token(char *token, int s, t_lexem **lexem, int nbr_line)
{
  char	*name;
  char	*parents;
  int	closed[2];
  int	nb;

  closed[0] = 0;
  closed[1] = nbr_line;
  if (token[0] != '/')
    {
      if ((name = get_token_name(token, s, nbr_line)) == NULL)
	return (1);
      verify_token(token, &s, &(closed[0]));
      parents = my_get_parents(lexem);
      if (my_put_in_list(lexem, name, closed, parents))
	return (1);
      nb = my_strlen(name) + 1;
      if ((s - nb) > 1)
	if (get_token_params(lexem, &token[nb], s - nb, nbr_line))
	  return (1);
    }
  else
    if (check_close(token, s, lexem, nbr_line))
      return (1);
  return (0);
}

int	detect_token(char *line, t_lexem **lexem, int nbr_line)
{
  int	i;
  int	s;

  if (!line[0])
    return (0);
  if (line[0] == '<')
    {
      if ((s = get_token(&line[0], nbr_line)) == -1)
	return (1);
      if (parse_token(&line[1], s, lexem, nbr_line))
	return (1);
      return (detect_token(&line[s + 2], lexem, nbr_line));
    }
  else
    {
      if (my_set_data(line, &i, lexem, nbr_line))
	return (1);
      if (line[i])
	return (detect_token(&line[i], lexem, nbr_line));
    }
  return (0);
}
