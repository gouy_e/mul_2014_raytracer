/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 19:25:05 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			object_shine(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  double		value;

  token = (*tmp);
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the shine.\n",
		  'w', token->line);
      value = 1.;
    }
  obj->shine = value;
  *tmp = token->prev;
  return (0);
}

int			object_opacity(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  double		value;

  token = (*tmp);
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the opacity.\n",
		  'w', token->line);
      value = 1.;
    }
  obj->opacity = 1. - value;
  *tmp = token->prev;
  return (0);
}

int			object_reflection(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  double		value;

  token = (*tmp);
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the reflection.\n",
		  'w', token->line);
      value = 1.;
    }
  obj->reflection = value;
  *tmp = token->prev;
  return (0);
}

int			object_refraction(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  double		value;

  token = (*tmp);
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the refraction.\n",
		  'w', token->line);
      value = 1.;
    }
  obj->refrac = value;
  *tmp = token->prev;
  return (0);
}

int			object_color(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  t_lexem_params	*params;
  double		value;

  token = (*tmp);
  params = token->params;
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the color.\n",
		  'w', token->line);
      value = 0xFFFFFF;
    }
  obj->color.color = value;
  while (params && params->name)
    {
      if (!my_strcmp("color2", params->name) && params->value)
	obj->color2.color = atof(params->value);
      params = params->next;
    }
  *tmp = token->prev;
  return (0);
}
