/*
** parse.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:42:58 2014
** Last update Tue May 13 12:16:24 2014 
*/

#include <stdlib.h>
#include "my.h"

int	is_valid_char(char c, char *pattern)
{
  int	i;

  i = -1;
  while (pattern[++i])
    if (c == pattern[i])
      return (1);
  return (0);
}

char	*concaten_strs(char *dest, char *str)
{
  char	*tmp;
  int	len;
  int	i;
  int	j;

  len = my_strlen(dest) + my_strlen(str) + 1;
  if ((tmp = malloc(sizeof(char) * len)) == NULL)
    return (NULL);
  i = -1;
  while (dest[++i])
    tmp[i] = dest[i];
  --i;
  j = -1;
  while (str[++j])
    tmp[++i] = str[j];
  tmp[++i] = 0;
  free(dest);
  free(str);
  return (tmp);
}
