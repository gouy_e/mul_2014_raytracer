/*
** parser.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 13:49:17 2014
** Last update Fri Jun  6 19:13:48 2014 
*/

#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "parse_functions.h"
#include "error.h"
#include "my.h"

void	init_func_tab_objects_2(t_parsing *parser)
{
  parser->objects[11].name = "limitmin";
  parser->objects[11].ptr = object_limitmin;
  parser->objects[12].name = "refraction";
  parser->objects[12].ptr = object_refraction;
  parser->objects[13].name = NULL;
}

void	init_func_tab_objects(t_parsing *parser)
{
  parser->objects[0].name = "position";
  parser->objects[0].ptr = object_position;
  parser->objects[1].name = "rotation";
  parser->objects[1].ptr = object_rotation;
  parser->objects[2].name = "radius";
  parser->objects[2].ptr = object_radius;
  parser->objects[3].name = "color";
  parser->objects[3].ptr = object_color;
  parser->objects[4].name = "shine";
  parser->objects[4].ptr = object_shine;
  parser->objects[5].name = "reflection";
  parser->objects[5].ptr = object_reflection;
  parser->objects[6].name = "opacity";
  parser->objects[6].ptr = object_opacity;
  parser->objects[7].name = "specularity";
  parser->objects[7].ptr = object_specularity;
  parser->objects[8].name = "texture";
  parser->objects[8].ptr = object_texture;
  parser->objects[9].name = "image";
  parser->objects[9].ptr = object_image;
  parser->objects[10].name = "limitmax";
  parser->objects[10].ptr = object_limitmax;
  init_func_tab_objects_2(parser);
}

void	init_func_tab_lights(t_parsing *parser)
{
  parser->lights[0].name = "position";
  parser->lights[0].ptr = light_position;
  parser->lights[1].name = "rotation";
  parser->lights[1].ptr = light_rotation;
  parser->lights[2].name = "color";
  parser->lights[2].ptr = light_color;
  parser->lights[3].name = "intensity";
  parser->lights[3].ptr = light_intensity;
  parser->lights[4].name = NULL;
}

void	init_func_tab_object_type(t_parsing *parser)
{
  parser->objtype[0].name = "sphere";
  parser->objtype[0].type = SPHERE;
  parser->objtype[1].name = "cylindre";
  parser->objtype[1].type = CYLINDRE;
  parser->objtype[2].name = "cone";
  parser->objtype[2].type = CONE;
  parser->objtype[3].name = "plan";
  parser->objtype[3].type = PLAN;
  parser->objtype[4].name = "cubetroue";
  parser->objtype[4].type = CUBETROUE;
  parser->objtype[5].name = "tore";
  parser->objtype[5].type = TORE;
  parser->objtype[6].name = NULL;
}
