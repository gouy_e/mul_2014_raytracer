/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 15:31:19 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

t_typelight		get_type_light(t_lexem_params *params,
				       t_parsing *prs, int nbr_line)
{
  t_typelight		type;
  int			i;

  type = SPOT;
  while (params && my_strcmp(params->name, "type"))
    params = params->next;
  i = -1;
  if (params)
    while (prs->ligtype[++i].name)
      if (params->value && !my_strcmp(prs->ligtype[i].name, params->value))
	return (prs->ligtype[i].type);
  print_error("Warning : The light's type is not initialized\n",
	      'w', nbr_line);
  return (type);
}

int			load_light_func(t_general *g,
				      t_lexem **token,
				      t_parsing *prs,
				      int *check)
{
  int			i;

  i = -1;
  *check = 0;
  while (prs->lights[++i].name)
    if (!my_strcmp(prs->lights[i].name, (*token)->name))
      {
	if (prs->lights[i].ptr(token, g->lights))
	  return (1);
	++(*check);
	i = 3;
      }
  return (0);
}

int			parse_light(t_general *g,
				     t_lexem **tmp,
				     t_parsing *prs)
{
  int			check;
  t_lexem		*token;
  t_typelight		type;

  token = (*tmp)->prev;
  type = get_type_light((*tmp)->params, prs, token->line);
  if (my_put_light_in_list(&(g->lights), type))
    return (1);
  while (token)
    {
      if (my_strcmp(token->parent, "light"))
  	{
  	  *tmp = token;
  	  return (0);
  	}
      if (load_light_func(g, &token, prs, &check))
	return (1);
      if (!check)
  	{
  	  print_xml_error(token->name, token->line);
  	  token = token->prev;
  	}
    }
  *tmp = token;
  return (0);
}
