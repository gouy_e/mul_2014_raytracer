/*
** get_next_line_file.c for minishell2 in /home/gouy_e/rendu/PSU_2013_minishell2/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Wed Mar  5 18:16:40 2014 gouy_e
** Last update Sat Mar  8 18:22:54 2014 gouy_e
*/

#include <stdlib.h>
#include <unistd.h>
#include "get_next_line.h"
#include "my.h"

int		my_strcat_on_buff(char *line, char *buff, char *save)
{
  int		i;
  int		j;

  j = 0;
  i = my_strlen(line);
  while (buff[j])
    {
      if (buff[j] == '\n' || i == BUF_SIZE)
        {
          line[i] = 0;
          save = my_strcpy(save, (buff + (++j)));
          return (1);
        }
      line[i++] = buff[j++];
    }
  save[0] = 0;
  line[i] = 0;
  return (0);
}

char		*get_next_line_file(const int fd)
{
  static char	save[BUF_SIZE];
  char		buff[BUF_SIZE + 1];
  char		*line;
  int		n;

  if ((line = malloc(sizeof(char) * BUF_SIZE)) == NULL)
    return (NULL);
  line[0] = 0;
  line[1] = 0;
  if (my_strcat_on_buff(line, save, save) == 1)
    return (line);
  while ((n = read(fd, buff, BUF_SIZE)) > 0)
    {
      buff[n] = 0;
      if (my_strcat_on_buff(line, buff, save) == 1)
        return (line);
    }
  if (line && line[0])
    return (line);
  free(line);
  return (NULL);
}
