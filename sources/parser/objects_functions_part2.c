/*
** parse_functions.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 17:17:00 2014
** Last update Fri Jun  6 19:23:13 2014 
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"
#include "struct_lexem.h"
#include "struct_parsing.h"
#include "manage_list.h"
#include "error.h"
#include "my.h"

int			object_radius(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  t_lexem_params	*params;
  double		value;

  token = (*tmp);
  params = token->params;
  if (token->value)
    value = atof(token->value);
  else
    {
      print_error("Warning : No value entered for the radius.\n",
		  'w', token->line);
      value = 1.;
    }
  obj->radius = value;
  while (params && params->name)
    {
      if (!my_strcmp("indice", params->name) && params->value)
	obj->indice = atof(params->value);
      params = params->next;
    }
  *tmp = token->prev;
  return (0);
}

int			object_image(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  t_lexem_params	*params;

  token = (*tmp);
  params = token->params;
  while (params)
    {
      if (!my_strcmp(params->name, "url") && params->value)
	{
	  if ((obj->imgurl = malloc(sizeof(char)
				    * (my_strlen(params->value) + 1)))
	      == NULL)
	    {
	      print_error("Warning : Malloc Failed, image not set.\n", 'w',
			  token->line);
	      *tmp = token->prev;
	      return (0);
	    }
	  obj->imgurl = my_strcpy(obj->imgurl, params->value);
	}
      params = params->next;
    }
  *tmp = token->prev;
  return (0);
}

int			texture_type(t_lexem_params *params, t_obj *obj)
{
  if (!my_strcmp(params->value, "perlin"))
    obj->texture = PERLIN;
  else if (!my_strcmp(params->value, "checkerboard"))
    obj->texture = CHECKERBOARD;
  else if (!my_strcmp(params->value, "spirale"))
    obj->texture = SPIRALE;
  return (0);
}

int			object_texture(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  t_lexem_params	*params;

  token = (*tmp);
  params = token->params;
  while (params)
    {
      if (!my_strcmp(params->name, "type") && params->value)
	texture_type(params, obj);
      else if (!my_strcmp(token->name, "x") && params->value)
	obj->options.x = atof(params->value);
      else if (!my_strcmp(token->name, "y") && params->value)
	obj->options.y = atof(params->value);
      else if (!my_strcmp(token->name, "z") && params->value)
	obj->options.z = atof(params->value);
      params = params->next;
    }
  *tmp = token->prev;
  return (0);
}

int			object_specularity(t_lexem **tmp, t_obj *obj)
{
  t_lexem		*token;
  t_lexem_params	*params;

  token = (*tmp);
  params = token->params;
  while (params)
    {
      if (!my_strcmp(params->name, "radius") && params->value)
	obj->specular_radius = atof(params->value);
      else if (!my_strcmp(params->name, "intensity") && params->value)
	obj->specular_intensity = atof(params->value);
      params = params->next;
    }
  *tmp = token->prev;
  return (0);
}
