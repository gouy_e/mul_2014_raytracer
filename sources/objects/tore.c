/*
** sphere.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:13:58 2014 Romain Hirt
** Last update Fri Jun  6 16:23:54 2014 
*/

#define _BSD_SOURCE

#include <pthread.h>
#include <math.h>
#include "struct_general.h"
#include "raytracer.h"
#include "color.h"
#include "rotation.h"
#include "my.h"

int		inter_tore(t_aff *aff,
			     t_general *g,
			     t_ray *ray,
			     t_obj *obj)
{
  t_coord	o;
  t_coord	pt;
  double	k;
  t_equ		e;
  t_coord	v;
  t_ray		newray;
  double	r1;
  double	r2;
  double	s1;
  double	s2;
  double	s3;
  double	w;

  r1 = obj->radius;
  r2 = obj->indice;

  v = ray->v;
  o = sub_vector(ray->coord, obj->coord);
  o = make_rotation(o, mult_vector(obj->rotate, 1.));
  v = make_rotation(v, mult_vector(obj->rotate, 1.));

  s1 = pow(v.x, 2.) + pow(v.y, 2.) + pow(v.z, 2.);
  s2 = 2. * (v.x * o.x + v.y * o.y + v.z * o.z);
  s3 = pow(o.x, 2.) + pow(o.y, 2.) + pow(o.z, 2.) + r1 * r1 - r2 * r2;
  e.a = s1 * s1;
  e.b = 2. * s1 * s2;
  e.c = 2. * s1 * s3 + s2 * s2 - 4 * r1 * r1 * (v.x * v.x + v.y * v.y);
  e.d = 2. * s3 * s2 - 8 * r1 * r1 * (v.x * o.x + v.y * o.y);
  e.e = s3 * s3 - 4. * r1 * r1 * (o.x * o.x + o.y * o.y);

  k = resolve_quad(e);
  if (k < 0. || (k > aff->k && aff->k > 0.))
    return (0);
  aff->pt = calc_inter_pt(ray, k);
  if (aff->pt.z - obj->coord.z > 1.01 * (r1 + r2) || aff->pt.x - obj->coord.x
      > 1.01 * (r1 + r2) || aff->pt.y - obj->coord.y > 1.01 * (r1 + r2))
    return (0);
  aff->ok = 1;
  aff->k = k;
  aff->color = obj->color;

  newray.v = v;
  newray.coord = o;
  pt = calc_inter_pt(&newray, k);
  w = 4. * (pow(pt.x, 2.) + pow(pt.y, 2.) + pow(pt.z, 2.)
	    + pow(r1, 2.) - pow(r2, 2.));
  aff->norm.x = pt.x * w - 8. * r1 * r1 * pt.x;
  aff->norm.y = pt.y * w - 8. * r1 * r1 * pt.y;
  aff->norm.z = pt.z * w;
  aff->norm = mult_vector(aff->norm, -1.);
  aff->norm = make_rotation(aff->norm, mult_vector(obj->rotate, -1.));

  aff->obj = obj;
  if (obj->reflection > 0. && g->nbr_ray < g->max_ray)
    make_reflection(aff, g, ray, obj);
  return (0);
}
