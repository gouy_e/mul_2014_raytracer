/*
** plan.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:13:58 2014 Romain Hirt
** Last update Sat Jun  7 16:29:06 2014 Romain Hirt
*/

#include <pthread.h>
#include "struct_general.h"
#include "raytracer.h"
#include "color.h"
#include "rotation.h"
#include "my.h"

int		make_checkerboard(t_aff *aff, t_obj *obj)
{
  int		i;
  int		j;

  i = (((int)aff->pt.x % 400 + 400) % 400) / 200;
  j = (((int)aff->pt.y % 400 + 400) % 400) / 200;
  if (i ^ j)
    aff->color.color = obj->color.color;
  else
    aff->color.color = obj->color2.color;
  return (0);
}

int		norm_plan(t_coord *norm, t_obj *obj)
{
  norm->x = 0;
  norm->y = 0;
  norm->z = -100;
  *norm = make_rotation(*norm, mult_vector(obj->rotate, -1.));
  return (0);
}

int		inter_plan(t_aff *aff,
			   __attribute__((unused))t_general *g,
			   t_ray *ray,
			   t_obj *obj)
{
  double	k;
  t_coord	o;
  t_coord	v;
  t_coord	norm;
  t_coord	pt;
  t_ray		newray;

  v = ray->v;
  o = sub_vector(ray->coord, obj->coord);
  o = make_rotation(o, mult_vector(obj->rotate, 1.));
  v = make_rotation(v, mult_vector(obj->rotate, 1.));
  k = -o.z / v.z;
  if (k < 0. || (k > aff->k && aff->k > 0.))
    return (0);
  norm_plan(&norm, obj);
  newray.coord = o;
  newray.v = v;
  pt = calc_inter_pt(&newray, k);
  if (check_limit(ray, k, obj))
    return (0);
  aff->norm = norm;
  aff->pt = pt;
  aff->ok = 1;
  aff->k = k;
  aff->color = obj->color;
  aff->obj = obj;
  if (obj->texture == CHECKERBOARD)
    make_checkerboard(aff, obj);
  aff->pt = calc_inter_pt(ray, k);
  return (0);
}
