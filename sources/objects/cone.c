/*
** sphere.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:13:58 2014 Romain Hirt
** Last update Sat Jun  7 15:18:08 2014 Romain Hirt
*/

#include <pthread.h>
#include <math.h>
#include "struct_general.h"
#include "raytracer.h"
#include "rotation.h"
#include "my.h"

int		inter_cone(t_aff *aff,
			   __attribute__((unused))t_general *g,
			   t_ray *ray,
			   t_obj *obj)
{
  t_coord	eq;
  t_coord	o;
  t_coord	v;
  double	k;
  double	a;

  a = pow(tan(obj->indice), 2.);
  v = ray->v;
  o = sub_vector(ray->coord, obj->coord);
  o = make_rotation(o, obj->rotate);
  v = make_rotation(v, obj->rotate);
  eq.x = (v.x * v.x) + (v.y * v.y) - (v.z * v.z) * a;
  eq.y = 2. * ((v.x * o.x) + (v.y * o.y) - (v.z * o.z) * a);
  eq.z = (o.x * o.x) + (o.y * o.y) - (o.z * o.z) * a;
  k = resolve_eq(eq);
  if (k < 0. || (k > aff->k && aff->k > 0.))
    return (0);
  aff->ok = 1;
  aff->k = k;
  aff->color = obj->color;
  aff->pt = calc_inter_pt(ray, k);
  aff->norm = sub_vector(obj->coord, aff->pt);
  aff->norm = make_rotation(aff->norm, obj->rotate);
  aff->norm.z *= -1. * obj->indice;
  aff->norm = make_rotation(aff->norm, mult_vector(obj->rotate, -1.));
  aff->obj = obj;
  return (0);
}
