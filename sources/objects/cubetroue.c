/*
** sphere.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:13:58 2014 Romain Hirt
** Last update Thu Jun  5 11:53:15 2014 
*/

#define _BSD_SOURCE

#include <math.h>
#include <pthread.h>
#include "struct_general.h"
#include "raytracer.h"
#include "color.h"
#include "rotation.h"
#include "my.h"

int		norm_cubetroue(t_aff *aff, t_obj *obj, t_coord pt)
{
  aff->norm.x = 4. * pow(pt.x, 3) - 10. * pt.x;
  aff->norm.y = 4. * pow(pt.y, 3) - 10. * pt.y;
  aff->norm.z = 4. * pow(pt.z, 3) - 10. * pt.z;
  aff->norm = mult_vector(aff->norm, -1.);
  aff->norm = make_rotation(aff->norm, mult_vector(obj->rotate, -1.));
  return (0);
}

int		calc_equation_cube(double *k, t_coord v, t_coord o, t_obj *obj)
{
  t_equ		eq;

  eq.a = pow(v.x, 4.) + pow(v.y, 4.) + pow(v.z, 4.);
  eq.b = 4. * (pow(v.x, 3.) * o.x + pow(v.y, 3.) * o.y + pow(v.z, 3.) * o.z);
  eq.c = 6. * (pow(v.x, 2.) * pow(o.x, 2.) + pow(v.y, 2.) * pow(o.y, 2.)
	    + pow(v.z, 2.) * pow(o.z, 2.)) - 5. * (pow(v.x, 2.) + pow(v.y, 2.)
						   + pow(v.z, 2.));
  eq.d = 4. * (pow(o.x, 3.) * v.x + pow(o.y, 3.) *
	       v.y + pow(o.z, 3.) * v.z) - 10.
    * (o.x * v.x + o.y * v.y + o.z * v.z);
  eq.e = pow(o.x, 4.) + pow(o.y, 4.) + pow(o.z, 4.) - 5.
    * (pow(o.x, 2.) + pow(o.y, 2.) + pow(o.z, 2.)) + obj->indice;
  *k = resolve_quad(eq);
  *k *= obj->radius;
  return (0);
}

int		inter_cubetroue(t_aff *aff,
				__attribute__((unused))t_general *g,
			     t_ray *ray,
			     t_obj *obj)
{
  t_ray		newray;
  t_coord	o;
  t_coord	v;
  t_coord	pt;
  double	k;

  v = ray->v;
  o = sub_vector(ray->coord, obj->coord);
  o = mult_vector(o, 1. / obj->radius);
  o = make_rotation(o, mult_vector(obj->rotate, 1.));
  v = make_rotation(v, mult_vector(obj->rotate, 1.));
  calc_equation_cube(&k, v, o, obj);
  if (k < 0. || (k > aff->k && aff->k > 0.))
    return (0);
  aff->pt = calc_inter_pt(ray, k);
  newray.v = v;
  newray.coord = o;
  pt = calc_inter_pt(&newray, k / obj->radius);
  aff->ok = 1;
  aff->k = k;
  aff->color = obj->color;
  norm_cubetroue(aff, obj, pt);
  aff->obj = obj;
  return (0);
}
