/*
** sphere.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:13:58 2014 Romain Hirt
** Last update Sat Jun  7 15:55:55 2014 Romain Hirt
*/

#include <pthread.h>
#include "struct_general.h"
#include "raytracer.h"
#include "rotation.h"
#include "my.h"

int		inter_cylindre(t_aff *aff,
			       __attribute__((unused))t_general *g,
			       t_ray *ray,
			       t_obj *obj)
{
  t_coord	eq;
  t_coord	o;
  t_coord	v;
  t_coord	norm;
  double	k;
  t_coord	pt;
  

  v = ray->v;
  o = sub_vector(ray->coord, obj->coord);
  o = make_rotation(o, mult_vector(obj->rotate, 1.));
  v = make_rotation(v, mult_vector(obj->rotate, 1.));
  eq.x = (v.x * v.x) + (v.y * v.y);
  eq.y = 2. * ((v.x * o.x) + (v.y * o.y));
  eq.z = (o.x * o.x) + (o.y * o.y) - (obj->radius * obj->radius);
  k = resolve_eq(eq);
  if (k < 0. || (k > aff->k && aff->k > 0.))
    return (0);
  pt = calc_inter_pt(ray, k);
  norm = sub_vector(obj->coord, pt);
  norm = make_rotation(norm, mult_vector(obj->rotate, 1.));
  norm.z = 0.;
  norm = make_rotation(norm, mult_vector(obj->rotate, -1.));
  if (check_limit(ray, k, obj))
    {
      k = resolve_eq_sec(eq);
      pt = calc_inter_pt(ray, k);
      if (k < 0. || (k > aff->k && aff->k > 0.) || check_limit(ray, k, obj))
        return (0);
      norm = mult_vector(sub_vector(obj->coord, pt), -1.);
    }
  aff->norm = norm;
  aff->pt = pt;
  aff->ok = 1;
  aff->k = k;
  aff->color = obj->color;
  aff->obj = obj;
  return (0);
}
