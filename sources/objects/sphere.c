/*
** sphere.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:13:58 2014 Romain Hirt
** Last update Sat Jun  7 15:51:48 2014 Romain Hirt
*/

#define _BSD_SOURCE

#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include "struct_general.h"
#include "raytracer.h"
#include "rotation.h"
#include "color.h"
#include "my.h"

int		make_mapping(t_aff *aff, t_obj *obj)
{
  int		pix;
  double	phi;
  double	u;
  double	v;
  double	theta;
  t_coord	vn;
  t_coord	ve;
  t_coord	vp;

  vp = mult_vector(aff->norm, 1. / obj->radius);
  vp = make_rotation(vp, obj->rotate);
  vn.x = 0.;
  vn.y = 0.;
  vn.z = 1;
  ve.x = 0.;
  ve.y = 1.;
  ve.z = 0.;
  phi = acos(-scal_vector(vn, vp));
  v = phi / M_PI;
  theta = (acos(scal_vector(vp, ve) / sin(phi))) / (2. * M_PI);
  if (scal_vector(cross_vector(vn, ve), vp) < 0.)
    u = theta;
  else
    u = 1. - theta;
  u *= (double)obj->img.x;
  v *= (double)obj->img.y;
  pix = (int)v * 4 * obj->img.x + (int)u * 4;
  aff->color.c[0] = obj->img.data[pix];
  aff->color.c[1] = obj->img.data[pix + 1];
  aff->color.c[2] = obj->img.data[pix + 2];
  aff->color.c[3] = obj->img.data[pix + 3];
  return (0);
}

int		inter_sphere(t_aff *aff,
			     t_general *g,
			     t_ray *ray,
			     t_obj *obj)
{
  t_coord	eq;
  t_coord	o;
  t_coord	v;
  t_coord	norm;
  t_coord	pt;
  double	k;

  g = g;
  v = ray->v;
  o = sub_vector(ray->coord, obj->coord);
  o = make_rotation(o, mult_vector(obj->rotate, 1.));
  v = make_rotation(v, mult_vector(obj->rotate, 1.));
  eq.x = (v.x * v.x) + (v.y * v.y) + (v.z * v.z);
  eq.y = 2. * ((v.x * o.x) + (v.y * o.y) + (v.z * o.z));
  eq.z = (o.x * o.x) + (o.y * o.y) + (o.z * o.z) - (obj->radius * obj->radius);
  k = resolve_eq(eq);
  if (k < 0. || (k > aff->k && aff->k > 0.))
    return (0);
  pt = calc_inter_pt(ray, k);
  norm = sub_vector(obj->coord, pt);
  if (check_limit(ray, k, obj))
    {
      k = resolve_eq_sec(eq);
      pt = calc_inter_pt(ray, k);
      if (k < 0. || (k > aff->k && aff->k > 0.) || check_limit(ray, k, obj))
	return (0);
      norm = mult_vector(sub_vector(obj->coord, pt), -1.);
    }
  aff->norm = norm;
  if (resolve_eq_sec(eq) < 0.)
    aff->isin = 1;
  else
    aff->isin = 0;
  aff->pt = pt;
  aff->ok = 1;
  aff->k = k;
  aff->color = obj->color;
  if (obj->imgurl != NULL)
    make_mapping(aff, obj);
  aff->obj = obj;
  return (0);
}
