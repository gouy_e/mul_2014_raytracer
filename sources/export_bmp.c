/*
** export_bmp.c for raytracer in /home/gouy_e/raytracer/sources
**
** Made by gouy_e
** Login   <gouy_e@epitech.net>
**
** Started on  Thu Jun  5 12:07:22 2014 gouy_e
** Last update Fri Jun  6 16:24:12 2014 
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include "struct_general.h"

void		put_int_header(char *head, int offset, int value)
{
 char		*tmp;

  tmp = (char *)&value;
  head[offset] = tmp[0];
  head[offset + 1] = tmp[1];
  head[offset + 2] = tmp[2];
  head[offset + 3] = tmp[3];
}

void		put_shortint_header(char *head, int offset, short int value)
{
  char		*tmp;

  tmp = (char *)&value;
  head[offset] = tmp[0];
  head[offset + 1] = tmp[1];
}

void		write_line(t_general *g, int j, int fd)
{
  int		i;

  i = j;
  while (i < j + g->win.screenx)
    {
      write(fd, g->image.data + i, 3);
      i += 4;
    }
}

int		export_img(t_general *g, int fd)
{
  char		header[54];
  int		j;

  j = -1;
  while (++j < 54)
    header[j] = 0;
  header[0] = 'B';
  header[1] = 'M';
  put_int_header(header, 2, g->win.screenx*g->win.screeny*3 + 54);
  put_int_header(header, 10, 54);
  put_int_header(header, 14, 40);
  put_int_header(header, 18, g->win.screenx);
  put_int_header(header, 22, g->win.screeny);
  put_shortint_header(header, 26, 24);
  write(fd, &header, 54);
  j = (g->win.screeny * g->win.screenx - g->win.screenx) * 4;
  while (j >= 0)
    {
      write_line(g, j, fd);
      j -= g->win.screenx * 4;
    }
  return (0);
}

int		export_bmp(t_general *g)
{
  int		fd;

  fd = open("salut.bmp", O_WRONLY | O_CREAT | O_TRUNC, 0644);
  export_img(g, fd);
  return (0);
}
