/*
** raytracer.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 14:58:58 2014 Romain Hirt
** Last update Fri Jun  6 16:18:14 2014 
*/

#define _BSD_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <mlx.h>
#include <math.h>
#include <pthread.h>
#include "struct_general.h"
#include "raytracer.h"
#include "rotation.h"
#include "object.h"
#include "thread.h"
#include "pixel.h"
#include "color.h"
#include "light.h"
#include "bmp.h"
#include "my.h"

int		call_func(t_aff *aff, t_general *g, t_obj *obj, t_ray *ray)
{
  int		i;

  i = -1;
  while (g->ptrobj[++i].type != NONE_O)
    if (obj->type == g->ptrobj[i].type)
      g->ptrobj[i].ptr(aff, g, ray, obj);
  return (0);
}

void		free_struct(t_obj *objs, t_cam *cams, t_light *lights)
{
  t_obj		*objs2;
  t_cam		*cams2;
  t_light	*lights2;

  while (objs)
    {
      objs2 = objs;
      objs = objs->next;
      free(objs2);
    }
  while (cams)
    {
      cams2 = cams;
      cams = cams->next;
      free(cams2);
    }
  while (lights)
    {
      lights2 = lights;
      lights = lights->next;
      free(lights2);
    }
}

void		my_exit(t_general *g)
{
  t_obj		*objs;
  t_cam		*cams;
  t_light	*lights;

  objs = g->objs;
  cams = g->cams;
  lights = g->lights;
  free_struct(objs, cams, lights);
  if (g->export)
    free(g->export);
  exit(0);
}

int     key_actions(int keycode,
		    __attribute__((unused))t_general *g)
{
  if (keycode == 65307)
    my_exit(g);
  return (0);
}

int		get_pixinfo(t_aff *aff, t_general *g, t_ray *ray, int n)
{
  t_obj		*obj;

  aff->k = -1;
  aff->ok = 0;
  obj = g->objs;
  while (obj != NULL)
    {
      call_func(aff, g, obj, ray);
      obj = obj->next;
    }
  if (aff->ok && aff->obj->reflection > 0. && g->nbr_ray < g->max_ray)
    make_reflection(aff, g, ray, aff->obj);
  if (aff->ok && aff->obj->opacity > 0. && g->nbr_ray < g->max_ray)
    make_transp(aff, g, ray, aff->obj);
  if (aff->ok == 1 && n)
    get_lumi(aff, g);
  return (0);
}

int		make_picture(t_general *g)
{
  t_obj		*tmp;

  tmp = g->objs;
  while (tmp != NULL)
    {
      if (tmp->imgurl != NULL)
	{
	  tmp->img.img = mlx_xpm_file_to_image(g->win.mlx_ptr, tmp->imgurl,
					       &(tmp->img.x), &(tmp->img.y));
	  if (tmp->img.img == NULL)
	    {
	      free(tmp->imgurl);
	      tmp->imgurl = NULL;
	    }
	  else
	    tmp->img.data = mlx_get_data_addr(tmp->img.img, &(tmp->img.bpp),
					      &(tmp->img.sizeline),
					      &(tmp->img.endian));
	}
      tmp = tmp->next;
    }
  return (0);
}

void		*ray_cast(void *ptr)
{
  t_general	*g;
  int		i;
  int		j;
  t_ray		ray;
  t_aff		aff;

  g = (t_general *)ptr;
  i = g->image.x;
  ray.coord = g->cams->coord;
  while (i < g->win.screenx)
    {
      j = -1;
      while (++j < g->win.screeny)
	{
	  ray.v.x = 100. - g->cams->coord.x;
	  ray.v.y = ((g->win.screenx / 2.) - i - g->cams->coord.y) * -1.;
	  ray.v.z = (g->win.screeny / 2.) - j - g->cams->coord.z;
	  g->nbr_ray = 0;
	  g->obj = NULL;
	  ray.v = make_rotation(ray.v, g->cams->rotate);
	  get_pixinfo(&aff, g, &ray, 1);
	  pixel_img(&aff, i, j, g);
	}
      i += g->thread_nbr;
      pthread_mutex_lock(g->lock);
      *(g->pourc) += g->win.screeny;
      j = *(g->pourc) / (g->win.screeny * g->win.screenx) * 100;
      my_printf("\rLoading : %d%", j);
      pthread_mutex_unlock(g->lock);
    }
  return (NULL);
}

void		init_ptrobj(t_general *g)
{
  g->ptrobj[0].type = SPHERE;
  g->ptrobj[0].ptr = inter_sphere;
  g->ptrobj[1].type = CONE;
  g->ptrobj[1].ptr = inter_cone;
  g->ptrobj[2].type = CYLINDRE;
  g->ptrobj[2].ptr = inter_cylindre;
  g->ptrobj[3].type = PLAN;
  g->ptrobj[3].ptr = inter_plan;
  g->ptrobj[4].type = CUBETROUE;
  g->ptrobj[4].ptr = inter_cubetroue;
  g->ptrobj[5].type = TORE;
  g->ptrobj[5].ptr = inter_tore;
  g->ptrobj[6].type = NONE_O;
}

void		init_ptrlight(t_general *g)
{
  g->ptrlight[0].type = SPOT;
  g->ptrlight[0].ptr = spot_lumi;
  g->ptrlight[1].type = DIFFUSE;
  g->ptrlight[1].ptr = diffuse_lumi;
  g->ptrlight[2].type = NONE_L;
}

int		init_window(t_general *g)
{
  if ((g->win.mlx_ptr = mlx_init()) == NULL)
    {
      my_puterror("Error during the initialization of the window.\n");
      return (1);
    }
  return (0);
}

int		open_window(t_general *g)
{
  g->win.win_ptr = mlx_new_window(g->win.mlx_ptr, g->win.screenx,
  				  g->win.screeny, "Raytracer de PGM");
  if (g->win.win_ptr == NULL)
    {
      my_puterror("Error : the window is not able to be opened.\n");
      return (1);
    }
  return (0);
}

int			raytracer(t_general *g)
{
  int			pourc;
  pthread_mutex_t	lock;

  init_ptrobj(g);
  init_ptrlight(g);
  if (init_window(g))
    return (1);
  g->image.img = mlx_new_image(g->win.mlx_ptr, g->win.screenx, g->win.screeny);
  if (g->image.img == NULL)
    {
      my_puterror("Error during the creation of the image.\n");
      return (1);
    }
  g->image.data = mlx_get_data_addr(g->image.img, &g->image.bpp,
				    &g->image.sizeline, &g->image.endian);
  make_picture(g);
  pourc = 0;
  g->pourc = &pourc;
  pthread_mutex_init(&lock, NULL);
  g->lock = &lock;
  thread_on_ray(g);
  export_bmp(g);
  open_window(g);
  mlx_expose_hook(g->win.win_ptr, my_aff_image, g);
  mlx_key_hook(g->win.win_ptr, key_actions, g);
  mlx_loop(g->win.mlx_ptr);
  return (0);
}
