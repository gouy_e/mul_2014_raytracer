/*
** color.c for Raytracer in /home/hirt_r/rendu/raytracer
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Fri May 23 14:17:00 2014 Romain Hirt
** Last update Tue Jun  3 18:08:21 2014 gouy_e
*/

#include <stdio.h>
#include <pthread.h>
#include "struct_general.h"

int	color_lumi(t_color *c, double l)
{
  c->c[0] = (double)c->c[0] * l;
  c->c[1] = (double)c->c[1] * l;
  c->c[2] = (double)c->c[2] * l;
  c->c[3] = (double)c->c[3] * l;
  return (0);
}

int	color_shiny(t_color *c, t_color lum, double l)
{
  c->c[0] = (double)c->c[0] * (1 - l) + (double)lum.c[0] * l;
  c->c[1] = (double)c->c[1] * (1 - l) + (double)lum.c[1] * l;
  c->c[2] = (double)c->c[2] * (1 - l) + (double)lum.c[2] * l;
  c->c[3] = (double)c->c[3] * (1 - l) + (double)lum.c[3] * l;
  return (0);
}

int	add_color(t_color *c, t_color lum)
{
  int	m;

  m = c->c[0] + lum.c[0];
  if (m >= 256)
    c->c[0] = 255;
  else
    c->c[0] = m;
  m = c->c[1] + lum.c[1];
  if (m >= 256)
    c->c[1] = 255;
  else
    c->c[1] = m;
  m = c->c[2] + lum.c[2];
  if (m >= 256)
    c->c[2] = 255;
  else
    c->c[2] = m;
  m = c->c[3] + lum.c[3];
  if (m >= 256)
    c->c[3] = 255;
  else
    c->c[3] = m;
  return (0);
}
