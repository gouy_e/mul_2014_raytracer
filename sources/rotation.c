/*
** rotation.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Thu May 29 17:50:12 2014 Romain Hirt
** Last update Tue Jun  3 18:09:11 2014 gouy_e
*/

#define _BSD_SOURCE

#include <math.h>
#include <pthread.h>
#include "struct_general.h"

t_coord         rota_x(t_coord v, double a)
{
  t_coord       rota;

  a = a * M_PI / 180.;
  rota.x = v.x;
  rota.y = v.y * cos(a) - v.z * sin(a);
  rota.z = v.y * sin(a) + v.z * cos(a);
  return (rota);
}

t_coord         rota_y(t_coord v, double a)
{
  t_coord       rota;

  a = a * M_PI / 180.;
  rota.x = cos(a) * v.x + sin(a) * v.z;
  rota.y = v.y;
  rota.z = cos(a) * v.z - sin(a) * v.x;
  return (rota);
}

t_coord         rota_z(t_coord v, double a)
{
  t_coord       rota;

  a = a * M_PI / 180.;
  rota.x = cos(a) * v.x - sin(a) * v.y;
  rota.y = sin(a) * v.x + cos(a) * v.y;
  rota.z = v.z;
  return (rota);
}

t_coord		make_rotation(t_coord v, t_coord r)
{
  t_coord	n;

  n = rota_z(v, r.z);
  n = rota_y(n, r.y);
  n = rota_x(n, r.x);
  return (n);
}
