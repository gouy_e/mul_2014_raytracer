/*
** pixel.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Mon May 19 16:52:04 2014 Romain Hirt
** Last update Thu Jun  5 16:03:03 2014 
*/

#include <mlx.h>
#include <pthread.h>
#include "struct_general.h"

int	my_aff_image(t_general *g)
{
  mlx_put_image_to_window(g->win.mlx_ptr, g->win.win_ptr, g->image.img, 0, 0);
  return (0);
}

void	put_pixel(int i, int j, t_general *g, t_color color)
{
  g->image.data[(i * 4) + (j * 4 * (int)g->win.screenx)] = color.c[0];
  g->image.data[(i * 4) + (j * 4 * (int)g->win.screenx) + 1] = color.c[1];
  g->image.data[(i * 4) + (j * 4 * (int)g->win.screenx) + 2] = color.c[2];
  g->image.data[(i * 4) + (j * 4 * (int)g->win.screenx) + 3] = color.c[3];
}

void	pixel_img(t_aff *aff, int i, int j, t_general *g)
{
  if (!aff->ok)
    aff->color.color = 0x000000;
  put_pixel(i, j, g, aff->color);
}
