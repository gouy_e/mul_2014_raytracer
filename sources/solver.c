/*
** solver.c for raytracer in /home/darnat_a/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue Jun  3 16:25:09 2014
** Last update Fri Jun  6 13:35:50 2014 gouy_e
*/

#define _BSD_SOURCE

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct_general.h"

void		resolve_2nd(double b, double c, double *x)
{
  double	delta;

  delta = (b * b) - 4. * c;
  if (delta > 0.)
    {
      x[0] = (-b + sqrt(delta)) / 2.;
      x[1] = (-b - sqrt(delta)) / 2.;
    }
  else
    {
      x[0] = -1.;
      x[1] = -1.;
    }
}

double		my_min(t_coord *x)
{
  if (x->x < x->y && x->x < x->z)
    return (x->x);
  else if (x->y < x->x && x->y < x->z)
    return (x->y);
  else
    return (x->z);
  return (0.);
}

double		resolve_3nd(double a2,
			    double a1,
			    double a0)
{
  double	s;
  t_coord	x;
  t_equ		sol;

  sol.a = ((a2 * a2) - 3. * a1) / 9.;
  sol.b = (a2 * (a2 * a2 - 4.5 * a1) + 13.5 * a0) / 27.;
  sol.c = pow(sol.a, 3.) - sol.b * sol.b;
  if (sol.c >= 0.)
    {
      sol.d = sol.b / pow(sol.a, 1.5);
      sol.e = acos(sol.d) / 3.;
      s = -2. * sqrt(sol.a);
      x.x = s * cos(sol.e) - a2 / 3.;
      x.y = s * cos(sol.e + 2. * M_PI / 3.) - a2 / 3.;
      x.z = s * cos(sol.e + 4. * M_PI / 3.) - a2 / 3.;
      return (my_min(&x));
    }
  s = pow(sqrt(-sol.c) + fabs(sol.b), 1. / 3.);
  if (sol.b < 0.)
    return (s + sol.a / s - a2 / 3.);
  else
    return (-s - sol.a / s - a2 / 3.);
  return (0.);
}

void	div_equ(t_equ *eq)
{
  eq->e /= eq->a;
  eq->d /= eq->a;
  eq->c /= eq->a;
  eq->b /= eq->a;
  eq->a /= eq->a;
}

void		resolve_4nd(t_equ eq, double *x)
{
  int		i;
  t_equ		sol;

  div_equ(&eq);
  sol.a = -3. * (eq.b * eq.b) / (8. * (eq.a * eq.a)) + eq.c / eq.a;
  sol.b = pow(eq.b / 2., 3.) / pow(eq.a, 3.) - (eq.b * eq.c / pow(eq.a, 2.))
    / 2 + eq.d / eq.a;
  sol.c = -3. * pow(eq.b / 4. * eq.a, 4.) + eq.c * pow(eq.b / 4., 2.)
    / pow(eq.a, 3.)
    - (eq.b * eq.d / pow(eq.a, 2.)) / 4. + eq.e / eq.a;
  sol.d = resolve_3nd(-sol.a, -4. * sol.c, 4. * sol.a * sol.c
			     - pow(sol.b, 2.));

  sol.e = sol.b / (2. * (sol.d - sol.a));
  resolve_2nd(sqrt(sol.d - sol.a), sol.d / 2. - sol.e
		   * sqrt(sol.d - sol.a), &x[0]);
  resolve_2nd(-(sqrt(sol.d - sol.a)), sol.d / 2. + sol.e
		   * sqrt(sol.d - sol.a), &x[2]);
  i = -1;
  while (++i < 4)
    x[i] = x[i] - eq.b / (4. * eq.a);
}

double		resolve_quad(t_equ eq)
{
  double	x[4];
  double	mem;
  int		i;

  mem = -1.;
  resolve_4nd(eq, &x[0]);
  if (x[0] < 0. && x[1] < 0. && x[2] < 0. && x[3] < 0.)
    return (-1);
  i = -1;
  while (++i < 4)
    if (x[i] > 0.)
      mem = x[i];
  i = -1;
  while (++i < 4)
    if (x[i] < mem && x[i] > 0.)
      mem = x[i];
  return (mem);
}
