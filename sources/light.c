/*
** light.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Fri May 23 17:21:18 2014 Romain Hirt
** Last update Sat Jun  7 15:04:33 2014 Romain Hirt
*/

#define _BSD_SOURCE

#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#include "struct_general.h"
#include "raytracer.h"
#include "color.h"

int		shadow_calc(t_aff shadow, t_color *c, t_color *spe)
{
  if (shadow.ok && shadow.k < 0.999999)
    {
      color_lumi(c, shadow.obj->opacity);
      color_lumi(spe, shadow.obj->opacity);
    }
  return (0);
}

int		diffuse_lumi(t_aff *aff,
			     t_general *g,
			     t_color *mem,
			     t_light *light)
{
  t_color	c;
  t_color	spe;
  t_aff		shadow;
  t_ray		ray;
  double	l;

  c = aff->color;
  spe = light->color;
  color_shiny(&c, light->color, aff->obj->shine);
  l = angle_vector(aff->norm, sub_vector(aff->pt, light->coord))
    * light->intensity;
  color_lumi(&c, l);
  l /= light->intensity;
  l = 1. - ((1. - l) * (1. - aff->obj->opacity));
  l = pow(l, (10. * (20. / aff->obj->specular_radius)))
    * aff->obj->specular_intensity;
  color_lumi(&spe, l);
  ray.coord = light->coord;
  ray.v = sub_vector(aff->pt, light->coord);
  get_pixinfo(&shadow, g, &ray, 0);
  shadow_calc(shadow, &c, &spe);
  add_color(mem, spe);
  add_color(mem, c);
  return (0);
}

int		spot_lumi(t_aff *aff,
			  t_general *g,
			  t_color *mem,
			  t_light *light)
{
  t_color	c;
  double	l;
  double	s;

  g = g;
  c = aff->color;
  color_shiny(&c, light->color, aff->obj->shine);
  l = angle_vector(aff->norm, sub_vector(aff->pt, light->coord))
    * light->intensity;
  s = angle_vector(light->rotate, sub_vector(aff->pt, light->coord));
  if (s < 0.99)
    l = 0.;
  color_lumi(&c, l);
  add_color(mem, c);
  return (0);
}

int		get_lumi(t_aff *aff, t_general *g)
{
  t_light	*light;
  t_color	mem;
  int		i;

  light = g->lights;
  mem.color = 0;
  while (light != NULL)
    {
      i = -1;
      while (g->ptrlight[++i].type != NONE_L)
	if (g->ptrlight[i].type == light->type)
	  g->ptrlight[i].ptr(aff, g, &mem, light);
      light = light->next;
    }
  aff->color = mem;
  return (0);
}
