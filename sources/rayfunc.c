/*
** rayfunc.c for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 18:24:25 2014 Romain Hirt
** Last update Fri Jun  6 16:21:58 2014 
*/

#define _BSD_SOURCE

#include <math.h>
#include <pthread.h>
#include <stdio.h>
#include "struct_general.h"
#include "raytracer.h"
#include "rotation.h"
#include "color.h"
#include "my.h"

int             check_limit(t_ray *ray, double k, t_obj *obj)
{
  t_ray         newray;
  t_coord       pt;
  t_coord	start;
  t_coord	end;

  /* start = add_vector(obj->limit.start, obj->coord); */
  /* end = add_vector(obj->limit.end, obj->coord); */
  start = obj->limit.start;
  end = obj->limit.end;
  newray.coord = sub_vector(ray->coord, obj->coord);
  newray.coord = make_rotation(newray.coord, obj->rotate);
  newray.v = make_rotation(ray->v, obj->rotate);
  pt = calc_inter_pt(&newray, k);
  pt = make_rotation(pt, obj->rotate);
  if ((obj->limit.startuse[0] && pt.x < start.x) ||
      (obj->limit.startuse[1] && pt.y < start.y) ||
      (obj->limit.startuse[2] && pt.z < start.z) ||
      (obj->limit.enduse[0] && pt.x > end.x) ||
      (obj->limit.enduse[1] && pt.y > end.y) ||
      (obj->limit.enduse[2] && pt.z > end.z))
    return (1);
  return (0);
}

double		resolve_eq(t_coord eq)
{
  double	delta;
  double	x1;
  double	x2;

  delta = (eq.y * eq.y) - (4 * eq.x * eq.z);
  if (delta < 0)
    return (-1.);
  else
    {
      x1 = (-eq.y - sqrt(delta)) / (2 * eq.x);
      x2 = (-eq.y + sqrt(delta)) / (2 * eq.x);
      if (x1 < 0. && x2 < 0.)
	return (-1.);
      else if (x1 > 0. && x2 < 0.)
	return (x1);
      else if (x2 > 0. && x1 < 0.)
	return (x2);
      else if (x1 < x2)
	return (x1);
      else
	return (x2);
    }
  return (0);
}

double		resolve_eq_sec(t_coord eq)
{
  double	delta;
  double	x1;
  double	x2;

  delta = (eq.y * eq.y) - (4 * eq.x * eq.z);
  if (delta < 0)
    return (-1.);
  else
    {
      x1 = (-eq.y - sqrt(delta)) / (2 * eq.x);
      x2 = (-eq.y + sqrt(delta)) / (2 * eq.x);
      if (x1 < 0. && x2 < 0.)
	return (-1.);
      else if (x1 > 0. && x2 < 0.)
	return (-1.);
      else if (x2 > 0. && x1 < 0.)
	return (-1.);
      else if (x1 < x2)
	return (x2);
      else
	return (x1);
    }
  return (0);
}

t_coord		sub_vector(t_coord a, t_coord b)
{
  a.x = a.x - b.x;
  a.y = a.y - b.y;
  a.z = a.z - b.z;
  return (a);
}

t_coord		add_vector(t_coord a, t_coord b)
{
  a.x = a.x + b.x;
  a.y = a.y + b.y;
  a.z = a.z + b.z;
  return (a);
}

t_coord		cross_vector(t_coord a, t_coord b)
{
  t_coord	c;

  c.x = a.y * b.z - a.z * b.y;
  c.y = a.z * b.x - a.x * b.z;
  c.z = a.x * b.y - a.y * b.x;
  return (c);
}

double		angle_vector(t_coord v1, t_coord v2)
{
  double	m;
  double	n;
  double	o;
  double	a;

  m = (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
  n = sqrt((v1.x * v1.x) + (v1.y * v1.y) + (v1.z * v1.z));
  o = sqrt((v2.x * v2.x) + (v2.y * v2.y) + (v2.z * v2.z));
  a = m / (n * o);
  if (a < 0.)
    return (0.);
  return (a);
}

t_coord		calc_inter_pt(t_ray *ray, double k)
{
  t_coord	v;

  v.x = ray->coord.x + (k * ray->v.x);
  v.y = ray->coord.y + (k * ray->v.y);
  v.z = ray->coord.z + (k * ray->v.z);
  return (v);
}

t_coord		mult_vector(t_coord v, double d)
{
  v.x *= d;
  v.y *= d;
  v.z *= d;
  return (v);
}

double		scal_vector(t_coord v1, t_coord v2)
{
  return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

int		make_reflection(t_aff *aff,
				t_general *g,
				t_ray *ray,
				t_obj *obj)
{
  t_ray		refray;
  t_aff		ref;
  double	mem;

  g->nbr_ray += 1;
  refray.coord = aff->pt;
  mem = (ray->v.x * aff->norm.x + ray->v.y * aff->norm.y + ray->v.z
	 * aff->norm.z) / (pow(aff->norm.x, 2.) + pow(aff->norm.y, 2.)
			   + pow(aff->norm.z, 2.));
  refray.v.x = 2. * aff->norm.x * mem - ray->v.x;
  refray.v.y = 2. * aff->norm.y * mem - ray->v.y;
  refray.v.z = 2. * aff->norm.z * mem - ray->v.z;
  refray.v = mult_vector(refray.v, -1);
  refray.coord = calc_inter_pt(&refray, 0.00000001);
  get_pixinfo(&ref, g, &refray, 1.);
  if (ref.ok)
    color_shiny(&aff->color, ref.color, obj->reflection);
  return (0);
}

int		make_transp(t_aff *aff, t_general *g, t_ray *ray, t_obj *obj)
{
  t_ray		refray;
  t_coord	norm;
  t_aff		ref;
  double	c1;
  double	c2;
  double	n;

  g->nbr_ray += 1;
  n = 1. / 1.33;
  norm = aff->norm;
  if (aff->isin)
    {
      /* my_putstr("A"); */
      /* norm = mult_vector(norm, -1.); */
      /* n = 1. / n; */
    }
  /* else */
  /*   my_putstr("B"); */

  norm = mult_vector(norm, -1.);

  /* printf("N = %f\n", n); */

  c1 = 0. - scal_vector(norm, ray->v);
  c2 = sqrt(1 - pow(n, 2.) * (1. - pow(c1, 2.)));
  if (c1 > 0.)
    refray.v = add_vector(mult_vector(ray->v, n), mult_vector(norm,
							      n * c1 - c2));
  else
    refray.v = add_vector(mult_vector(ray->v, n), mult_vector(norm,
							      n * c1 - c2));

  refray.coord = aff->pt;
  refray.coord = calc_inter_pt(&refray, 0.000001);
  refray.v.x = 100.;
  refray.v.y = 0.;
  refray.v.z = 0.;
  get_pixinfo(&ref, g, &refray, 1.);

  /* printf("PT : X = %f | Y = %f | Z = %f\n",
     aff->pt.x, aff->pt.y, aff->pt.z); */
  /* printf("V : X = %f | Y = %f | Z = %f\n",
     ray->v.x, ray->v.y, ray->v.z); */
  /* printf("R : X = %f | Y = %f | Z = %f\n",
     refray.v.x, refray.v.y, refray.v.z);   */
  /* printf("NP : X = %f | Y = %f | Z = %f\n\n\n",
     ref.pt.x, ref.pt.y, ref.pt.z); */

  if (!ref.ok)
    ref.color.color = 0x000000;
  color_shiny(&aff->color, ref.color, obj->opacity);
  return (0);
}
