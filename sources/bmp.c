/*
** bmp.c for raytracer in /home/gouy_e/raytracer/sources
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Thu Jun  5 17:14:16 2014 gouy_e
** Last update Thu Jun  5 17:58:51 2014 gouy_e
*/

#include <sys/stat.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "struct_general.h"
#include "bmp.h"
#include "my.h"

void		init_struct(char *ptr, int len)
{
  int		i;

  i = -1;
  while (++i < len)
    ptr[i] = 0;
}

void		set_bitmap(t_bitmap *bitmap, t_general *g)
{
  bitmap->size = (DWORD)108;
  bitmap->width = (DWORD)g->win.screenx;
  bitmap->height = (DWORD)(-1 * g->win.screeny);
  bitmap->planes = (WORD)1;
  bitmap->bpp = (WORD)g->image.bpp;
  bitmap->compression = (DWORD)0;
  bitmap->size_of_bitmap = (DWORD)0;
  bitmap->horz_resolution = (LONG)1.;
  bitmap->vert_resolution = (LONG)1.;
  bitmap->colors_used = (DWORD)0;
  bitmap->colors_important = (DWORD)0;
  bitmap->blue_mask = (DWORD)0xFF000000;
  bitmap->green_mask = (DWORD)0x00FF0000;
  bitmap->red_mask = (DWORD)0x0000FF00;
  bitmap->alpha_mask = (DWORD)0x000000FF;
  bitmap->c_s_type = (DWORD)1;
}

int		export_img(t_general *g, int fd)
{
  WORD		tmp;
  DWORD		temp;
  t_bitmap	bitmap;
  int		len;
  int		res;

  res = 0;
  init_struct((char *)&bitmap, sizeof(t_bitmap));
  set_bitmap(&bitmap, g);
  tmp = 0x4D42;
  len = g->win.screenx * g->win.screeny * 4;
  if (write(fd, &tmp, 2) != 2)
    res = 1;
  temp = len + 122;
  if (write(fd, &temp, 4) != 4)
    res = 1;
  temp = 0;
  if (write(fd, &temp, 4) != 4)
    res = 1;
  temp = 122;
  if (write(fd, &temp, 4) != 4 ||
      write(fd, &bitmap, sizeof(t_bitmap)) != sizeof(t_bitmap) ||
      write(fd, g->image.data, len) != len)
    res = 1;
  return (res);
}

int		export_bmp(t_general *g)
{
  int           fd;

  if (g->export == NULL)
    return (0);
  if ((fd = open(g->export, O_WRONLY | O_CREAT | O_TRUNC, 0644)) == -1)
    return (-1);
  if (export_img(g, fd) == 0)
    print_success("Image Successfully exported.\n");
  else
    print_error("Image Unsuccessfully exported.\n");
  close(fd);
  return (0);
}
