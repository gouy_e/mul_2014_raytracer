/*
** main.c for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 15:44:54 2014
** Last update Wed Jun  4 19:39:51 2014 gouy_e
*/

#include <sys/stat.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "struct_general.h"
#include "raytracer.h"
#include "parse.h"
#include "my.h"

int		launch_raytracer(char *file)
{
  int		fd;
  t_general	g;

  if ((fd = open(file, O_RDWR)) == -1)
    {
      if (my_puterror("Error : Unable to read the file\n") == -1)
	return (1);
      return (1);
    }
  if (parse_file(fd, &g))
    return (1);
  if (close(fd) == -1)
    return (1);
  if (raytracer(&g))
    return (1);
  return (0);
}

int		main(int ac, char **av)
{
  if (ac == 1)
    {
      if (my_puterror("Usage : ") == -1)
	return (EXIT_FAILURE);
      if (my_puterror(av[0]) == -1)
	return (EXIT_FAILURE);
      if (my_puterror(" [FILE]\n") == -1)
	return (EXIT_FAILURE);
    }
  else
    {
      if (launch_raytracer(av[1]))
      	return (EXIT_FAILURE);
    }
  return (EXIT_SUCCESS);
}
