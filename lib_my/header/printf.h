/*
** printf.h for gouy_e in /home/gouy_e/rendu/PSU_2013_my_ls/lib/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Fri Nov 29 19:29:53 2013 gouy_e
** Last update Tue Mar  4 13:30:56 2014 gouy_e
*/

#ifndef PRINTF_H_
# define PRINTF_H_

#include <stdarg.h>

struct		s_flag
{
  char		*data;
  int		(*fptr)(va_list);
};

typedef struct s_flag t_flag;

#endif /* !PRINTF_H_ */
