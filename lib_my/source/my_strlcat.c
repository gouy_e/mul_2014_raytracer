/*
** my_strlcat.c for my_strlcat.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:00:23 2014 gouy_e
** Last update Tue Mar  4 13:56:22 2014 gouy_e
*/

#include "my.h"

int		my_strlcat(char *dest, char *src, int size)
{
  int		j;
  int		f;
  int		nbr;
  int		i;
  int		m;

  f = size;
  j = -1;
  i = 0;
  m = my_strlen(dest);
  while (src[++j] > 0)
    {
      dest[size] = src[j];
      ++size;
      ++i;
    }
  nbr = f + i;
  if (m < f)
    nbr = nbr - (f - i) + 1;
  return (nbr);
}
