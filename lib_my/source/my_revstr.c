/*
** my_revstr.c for my_revstr.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:52:37 2014 gouy_e
** Last update Tue Jan 21 15:13:13 2014 gouy_e
*/

#include "my.h"

char	*my_revstr(char *str)
{
  int	count;
  char	trig;
  int	i;
  int	half;

  count = my_strlen(str);
  if (count % 2 == 0)
    half = count / 2;
  else
    half = (count + 1) / 2;
  i = -1;
  while (++i < half)
    {
      trig = str[i];
      str[i] = str[count - i - 1];
      str[count - i - 1] = trig;
    }
  return (str);
}
