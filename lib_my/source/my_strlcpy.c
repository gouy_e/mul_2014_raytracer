/*
** my_strlcpy.c for my_strlcpy.c in /home/gouy_e/rendu/PSU_2013_minishell2/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Fri Feb  7 11:10:36 2014 gouy_e
** Last update Mon Feb 10 18:57:43 2014 gouy_e
*/

char	*my_strlcpy(char *dest, char *src, int n)
{
  int	i;
  int	j;

  i = n - 1;
  j = -1;
  while (src[++i])
    dest[++j] = src[i];
  dest[++j] = 0;
  return (dest);
}
