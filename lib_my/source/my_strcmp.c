/*
** my_strcmp.c for my_strcmp.c in /home/Elliot/rendu/Piscine-C-Jour_06/ex_05
**
** Made by Gouy Elliot
** Login   <Elliot@epitech.net>
**
** Started on  Mon Oct  7 18:48:22 2013 Gouy Elliot
** Last update Sat May 17 13:08:05 2014 
*/

#include "my.h"

int		my_strcmp(char *a1, char *a2)
{
  while (*a1 && *a2 && *a1 == *a2 && ++a1 && ++a2);
  return (*a2 - *a1);
}
