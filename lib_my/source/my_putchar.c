/*
** my_putchar.c for my_putchar.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 14:51:32 2014 gouy_e
** Last update Tue Mar  4 13:40:49 2014 gouy_e
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}
