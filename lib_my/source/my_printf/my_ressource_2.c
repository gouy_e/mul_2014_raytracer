/*
** my_ressource_2.c for my_ressource_3.c in /home/Elliot/rendu/PSU_2013_my_printf/source
** 
** Made by Gouy Elliot
** Login   <Elliot@epitech.net>
** 
** Started on  Wed Nov 13 15:59:36 2013 Gouy Elliot
** Last update Tue Mar  4 13:53:36 2014 gouy_e
*/

#include <stdlib.h>
#include "my.h"

void		my_put_nbr_long_2(long int nb)
{
  if (nb < 0)
    {
      my_putchar('-');
      nb = nb * (-1);
    }
  if (nb >= 10)
    {
      my_put_nbr_long_2(nb / 10);
    }
  my_putchar(nb % 10 + '0');
}

int		my_put_nbr_long(va_list aq)
{
  long int	nbr;
  int		i;

  i = 0;
  nbr = va_arg(aq, long int);
  my_put_nbr_long_2(nbr);
  while (nbr >= 1 || nbr <= -1)
    {
      nbr = nbr / 10;
      i = i + 1;
    }
  return (i);
}

int		my_put_octal(int nbr)
{
  int		i;
  char		*result;
  char		*base;

  i = 0;
  base = "01234567";
  result = malloc(sizeof(char) * my_intlen(nbr));
  if (result == NULL)
    return (-1);
  while (nbr >= 1)
    {
      result[i] = base[(nbr % 8)];
      i = i + 1;
      nbr = nbr / 8;
    }
  result = my_revstr(result);
  result[i] = '\0';
  if (my_strlen(result) == 2)
    my_putchar('0');
  else if (my_strlen(result) == 1)
    my_putstr("00");
  my_putstr(result);
  return (4);
}

int		my_put_printablestr(va_list aq)
{
  int		i;
  int		a;
  int		b;
  char		*str;

  i = 0;
  b = 0;
  str = va_arg(aq, char *);
  while (str[i] != 0)
    {
      if (str[i] < 32 || str[i] >= 127)
	{
	  my_putchar('\\');
	  a = str[i];
	  b = b + my_put_octal(a);
	}
      else
	{
	  b = b + 1;
	  my_putchar(str[i]);
	}
      i = i + 1;
    }
  return (b);
}

int		my_get_nbr_base_bin(va_list aq)
{
  char		*result;
  int		i;
  char		*base;
  int		nbr;

  nbr = va_arg(aq, int);
  result = malloc(sizeof(char) * my_intlen(nbr));
  if (result == NULL)
    return (-1);
  i = 0;
  base = "01";
  while (nbr >= 1)
    {
      result[i] = base[(nbr % 2)];
      i = i + 1;
      nbr = nbr / 2;
    }
  result = my_revstr(result);
  result[i] = '\0';
  my_putstr(result);
  return (my_strlen(result));
}
