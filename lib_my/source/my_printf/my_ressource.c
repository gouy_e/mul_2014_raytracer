/*
** my_ressource.c for my_ressource.c in /home/Elliot/rendu/PSU_2013_my_printf/source
** 
** Made by Gouy Elliot
** Login   <Elliot@epitech.net>
** 
** Started on  Wed Nov 13 15:58:28 2013 Gouy Elliot
** Last update Tue Mar  4 13:51:47 2014 gouy_e
*/

#include <stdlib.h>
#include <stdarg.h>
#include "my.h"

int	my_intlen(int nbr)
{
  int	i;

  i = 0;
  while (nbr >= 1 || nbr <= -1)
    {
      nbr = nbr / 10;
      i = i + 1;
    }
  return (i);
}

int		my_get_nbr_base_hex(va_list aq)
{
  char		*result;
  int		i;
  char		*base;
  int		nbr;

  nbr = va_arg(aq, int);
  base = "0123456789abcdef";
  result = malloc(sizeof(char) * my_intlen(nbr));
  if (result == NULL)
    return (-1);
  i = 0;
  while (nbr >= 1)
    {
      result[i] = base[(nbr % 16)];
      i = i + 1;
      nbr = nbr / 16;
    }
  result = my_revstr(result);
  result[i] = '\0';
  my_putstr(result);
  return (my_strlen(result));
}

int		my_get_nbr_base_HEX(va_list aq)
{
  char		*result;
  int		i;
  char		*base;
  int		nbr;

  nbr = va_arg(aq, int);
  base = "0123456789ABCDEF";
  result = malloc(sizeof(char) * my_intlen(nbr));
  if (result == NULL)
    return (-1);
  i = 0;
  while (nbr >= 1)
    {
      result[i] = base[(nbr % 16)];
      i = i + 1;
      nbr = nbr / 16;
    }
  result = my_revstr(result);
  result[i] = '\0';
  my_putstr(result);
  return (my_strlen(result));
}

int		my_get_nbr_base_oct(va_list aq)
{
  char		*result;
  int		i;
  char		*base;
  int		nbr;

  nbr = va_arg(aq, int);
  result = malloc(sizeof(char) * my_intlen(nbr));
  if (result == NULL)
    return (-1);
  i = 0;
  base = "01234567";
  while (nbr >= 1)
    {
      result[i] = base[(nbr % 8)];
      i = i + 1;
      nbr = nbr / 8;
    }
  result = my_revstr(result);
  result[i] = '\0';
  my_putstr(result);
  return (my_strlen(result));
}

int		my_get_nbr_base_addr(va_list aq)
{
  char		*result;
  int		i;
  char		*base;
  long int	nbr;

  nbr = va_arg(aq, long int);
  base = "0123456789abcdef";
  result = malloc(sizeof(char) * my_intlen(nbr));
  if (result == NULL)
    return (-1);
  i = 0;
  while (nbr >= 1)
    {
      result[i] = base[(nbr % 16)];
      i = i + 1;
      nbr = nbr / 16;
    }
  result = my_revstr(result);
  result[i] = '\0';
  if (result[0] == '7')
    my_putstr("0x");
  else
    my_putstr("(nil)");
  my_putstr(result);
  return (my_strlen(result) + 2);
}
