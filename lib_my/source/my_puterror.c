/*
** my_puterror.c for my_puterror.c in /home/gouy_e/rendu/Minishell1.1/lib_my/source
**
** Made by gouy_e
** Login   <gouy_e@epitech.net>
**
** Started on  Wed Jan 22 11:41:25 2014 gouy_e
** Last update Thu May  8 15:51:15 2014 
*/

#include <unistd.h>
#include "my.h"

int		my_puterror(char *str)
{
  return (write(2, str, my_strlen(str)));
}
