/*
** epur_str.c for lib_my in /home/gouy_e/rendu/PSU_2013_minishell2/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Thu Mar  6 17:46:07 2014 gouy_e
** Last update Thu Mar  6 17:46:23 2014 gouy_e
*/

void		my_epurstr(char *str)
{
  int		i;
  int		j;

  i = -1;
  j = -1;
  while (str[++i])
    {
      if ((str[i] != ' ' && str[i] != '\t') ||
          ((str[i + 1] != ' ' && str[i + 1] != '\t') &&
           str[i + 1]))
        {
          if (str[i] == '\t')
            str[++j] = ' ';
          else
            str[++j] = str[i];
        }
    }
  str[++j] = 0;
  i = -1;
  j = -1;
  while (str[++i] && (str[i] == ' ' || str[i] == '\t'));
  --i;
  while (str[++i])
    str[++j] = str[i];
  str[++j] = str[i];
}
