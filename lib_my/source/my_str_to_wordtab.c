/*
** my_str_to_wordtab.c for my_str_to_wordtab.c in /home/gouy_e/rendu/lib_my
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Fri Dec 13 14:12:45 2013 gouy_e
** Last update Tue Mar  4 13:46:40 2014 gouy_e
*/

#include <stdlib.h>

int	cword(char *str, char c)
{
  int	i;
  int	j;

  i = -1;
  j = 1;
  while (str[++i])
    {
      if (str[i] == c)
        ++j;
    }
  return (j);
}

int	my_strnlen(char *str, char c)
{
  int	i;

  i = -1;
  while (str[++i] && str[i] != c);
  return (i);
}

char	**my_str_to_wordtab(char *str)
{
  char	**result;
  int	i;
  int	j;
  int	count;

  i = -1;
  count = -1;
  if (str == NULL)
    return (NULL);
  if ((result = malloc((cword(str, ' ') + 1) * sizeof(*result))) == NULL)
    return (NULL);
  while (++i < cword(str, ' ') && (j = -1) == -1)
    {
      while (str[++count] == ' ');
      if ((result[i] = malloc(my_strnlen(&str[count], ' ') + 2)) == NULL)
	return (NULL);
      --count;
      while (str[++count] && str[count] != ' ')
	result[i][++j] = str[count];
      result[i][j + 1] = '\0';
    }
  result[i] = NULL;
  return (result);
}
