/*
** my_strlen.c for my_strlen.c in /home/gouy_e/rendu/lib_my/source
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jan 21 15:00:03 2014 gouy_e
** Last update Tue Mar  4 13:56:47 2014 gouy_e
*/

int		my_strlen(char *str)
{
  int		m;

  m = -1;
  while (str[++m]);
  return (m);
}
