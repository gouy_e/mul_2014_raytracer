/*
** my_putstr.c for my_putstr.c in /home/gouy_e/rendu/lib_my/source
**
** Made by gouy_e
** Login   <gouy_e@epitech.net>
**
** Started on  Tue Jan 21 14:52:16 2014 gouy_e
** Last update Thu May  8 15:51:44 2014 
*/

#include <unistd.h>
#include "my.h"

int	my_putstr(char *str)
{
  return (write(1, str, my_strlen(str)));
}
