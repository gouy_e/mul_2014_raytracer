/*
** thread.h for raytracer in /home/gouy_e/raytracer/headers
** 
** Made by gouy_e
** Login   <gouy_e@epitech.net>
** 
** Started on  Tue Jun  3 16:58:29 2014 gouy_e
** Last update Tue Jun  3 16:59:22 2014 gouy_e
*/

#ifndef THREAD_H_
#define THREAD_H_

int	thread_on_ray(t_general *);

#endif /* !THREAD_H_ */
