/*
** light.h for Raytracer in /home/hirt_r/rendu/raytracer
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Fri May 23 18:00:42 2014 Romain Hirt
** Last update Fri May 23 18:07:48 2014 Romain Hirt
*/

#ifndef LIGHT_H_
# define LIGHT_H_

int	spot_lumi(t_aff *, t_general *, t_color *, t_light *);
int	diffuse_lumi(t_aff *, t_general *, t_color *, t_light *);

#endif /*!LIGHT_H_*/
