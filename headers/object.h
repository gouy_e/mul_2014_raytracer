/*
** object.h for Raytracer in /home/hirt_r/rendu/raytracer
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Mon May 19 16:19:23 2014 Romain Hirt
** Last update Wed Jun  4 19:09:12 2014 Romain Hirt
*/

#ifndef OBJECT_H_
# define OBJECT_H_

int	inter_sphere(t_aff *, t_general *, t_ray *, t_obj *);
int	inter_plan(t_aff *, t_general *, t_ray *, t_obj *);
int	inter_cone(t_aff *, t_general *, t_ray *, t_obj *);
int	inter_tore(t_aff *, t_general *, t_ray *, t_obj *);
int	inter_cylindre(t_aff *, t_general *, t_ray *, t_obj *);
int	inter_cubetroue(t_aff *, t_general *, t_ray *, t_obj *);

#endif /*!OBJECT_H_*/
