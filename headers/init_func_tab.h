/*
** init_func_tab.h for init_func_tab in /home/darnat_a/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri Jun  6 19:09:57 2014
** Last update Fri Jun  6 19:15:30 2014 
*/

#ifndef INIT_FUNC_TAB_H_
# define INIT_FUNC_TAB_H_

void	init_func_tab(t_parsing *);
void	init_func_tab_objects(t_parsing *);
void	init_func_tab_lights(t_parsing *);
void	init_func_tab_object_type(t_parsing *);

#endif /*!INIT_FUNC_TAB_H_*/
