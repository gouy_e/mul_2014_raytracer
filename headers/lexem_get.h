/*
** lexem_check.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue May 13 12:29:43 2014
** Last update Sat May 17 18:05:37 2014 
*/

#ifndef LEXEM_GET_H_
# define LEXEM_GET_H_

char	*check_params_value(char *, int);

#endif /*!LEXEM_GET_H_*/
