/*
** struct_general.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:46:01 2014
** Last update Fri Jun  6 18:37:01 2014 
*/

#ifndef STRUCT_GENERAL_H_
# define STRUCT_GENERAL_H_

typedef enum			e_typeobj
  {
    SPHERE			= 0,
    CYLINDRE,
    CONE,
    PLAN,
    CUBETROUE,
    TORE,
    NONE_O
  }				t_typeobj;

typedef enum			e_typelight
  {
    SPOT			= 0,
    DIFFUSE,
    NONE_L
  }				t_typelight;

typedef enum			e_texture
  {
    PERLIN			= 0,
    SPIRALE,
    CHECKERBOARD,
    NONE_T
  }				t_texture;

typedef union			u_color
{
  int				color;
  unsigned char			c[4];
}				t_color;

typedef struct			s_coord
{
  double			x;
  double			y;
  double			z;
}				t_coord;

typedef struct			s_equ
{
  double			a;
  double			b;
  double			c;
  double			d;
  double			e;
}				t_equ;

typedef struct			s_image
{
  void				*img;
  char				*data;
  int				bpp;
  int				sizeline;
  int				endian;
  int				x;
  int				y;
}				t_image;

typedef	struct			s_limit
{
  char				startuse[3];
  t_coord			start;
  char				enduse[3];
  t_coord			end;
}				t_limit;

typedef struct			s_obj
{
  t_coord			coord;
  t_coord			rotate;
  t_coord			options;
  t_color			color;
  t_color			color2;
  t_limit			limit;
  t_image			img;
  t_texture			texture;
  t_typeobj			type;
  char				*imgurl;
  double			indice;
  double			refrac;
  double			radius;
  double			opacity;
  double			shine;
  double			reflection;
  double			specular_intensity;
  double			specular_radius;
  struct s_obj			*next;
}				t_obj;

typedef struct			s_ray
{
  t_coord			coord;
  t_coord			v;
  t_color			color;
}				t_ray;

typedef struct			s_light
{
  t_coord			coord;
  t_coord			rotate;
  t_color			color;
  double			intensity;
  t_typelight			type;
  struct s_light		*next;
}				t_light;

typedef struct			s_cam
{
  t_coord			coord;
  t_coord			rotate;
  struct s_cam			*next;
  struct s_cam			*prev;
}				t_cam;

typedef struct			s_aff
{
  int				ok;
  int				isin;
  double			k;
  t_color			color;
  t_coord			norm;
  t_coord			pt;
  t_obj				*obj;
}				t_aff;

typedef struct			s_window
{
  void				*mlx_ptr;
  void				*win_ptr;
  double			screenx;
  double			screeny;
}				t_window;

typedef struct			s_error
{
  int				line;
  int				nbr_error;
  int				nbr_warning;
}				t_error;

typedef struct	s_general	t_general;

typedef struct			s_ptrobj
{
  t_typeobj			type;
  int				(*ptr)(t_aff *, t_general *, t_ray *, t_obj *);
}				t_ptrobj;

typedef struct			s_ptrlight
{
  t_typelight			type;
  int				(*ptr)(t_aff *, t_general *,
				       t_color *, t_light *);
}				t_ptrlight;

struct				s_general
{
  pthread_mutex_t		*lock;
  t_window			win;
  t_cam				*cams;
  t_obj				*objs;
  t_light			*lights;
  t_error			error;
  t_image			image;
  t_ptrobj			ptrobj[7];
  t_ptrlight			ptrlight[3];
  char				*export;
  int				nbr_ray;
  int				max_ray;
  int				thread_nbr;
  int				*pourc;
  t_obj				*obj;
};

#endif /*!STRUCT_GENERAL_H_*/
