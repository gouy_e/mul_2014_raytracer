/*
** type_light.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Sat May 17 12:37:48 2014
** Last update Sat May 17 12:38:30 2014 
*/

#ifndef TYPE_LIGHT_H_
# define TYPE_LIGHT_H_

t_typelight	type_spot();
t_typelight	type_diffuse();

#endif /*!TYPE_LIGHT_H_*/
