/*
** parse_functions.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri May 16 15:40:01 2014
** Last update Thu Jun  5 21:51:36 2014 
*/

#ifndef PARSE_FUNCTIONS_H_
# define PARSE_FUNCTIONS_H_

int	light_position(t_lexem **, t_light *);
int	light_rotation(t_lexem **, t_light *);
int	light_color(t_lexem **, t_light *);
int	light_intensity(t_lexem **, t_light *);
int	object_limitmin(t_lexem **, t_obj *);
int	object_limitmax(t_lexem **, t_obj *);
int	object_position(t_lexem **, t_obj *);
int	object_rotation(t_lexem **, t_obj *);
int	object_radius(t_lexem **, t_obj *);
int	object_color(t_lexem **, t_obj *);
int	object_shine(t_lexem **, t_obj *);
int	object_reflection(t_lexem **, t_obj *);
int	object_refraction(t_lexem **, t_obj *);
int	object_opacity(t_lexem **, t_obj *);
int	object_specularity(t_lexem **, t_obj *);
int	object_image(t_lexem **, t_obj *);
int	object_texture(t_lexem **, t_obj *);
int	camera_position(t_lexem **, t_cam *);
int	camera_rotation(t_lexem **, t_cam *);
int	parse_light(t_general *, t_lexem **, t_parsing *);
int	parse_camera(t_general *, t_lexem **, t_parsing *);
int	parse_object(t_general *, t_lexem **, t_parsing *);
int	parse_raytracer(t_general *, t_lexem **, t_parsing *);
int	raytracer_screenx(t_general *, char *, int);
int	raytracer_screeny(t_general *, char *, int);
int	raytracer_thread(t_general *, char *, int);
int	raytracer_cluster(t_general *, char *, int);
int	raytracer_nbray(t_general *, char *, int);
int	raytracer_export(t_general *, char *, int);

#endif /*!PARSE_FUNCTIONS_H_*/
