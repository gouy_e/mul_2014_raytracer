/*
** manage_list.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 20:55:30 2014
** Last update Sat May 17 17:52:31 2014 
*/

#ifndef MANAGE_LIST_H_
# define MANAGE_LIST_H_

int	my_put_light_in_list(t_light **, t_typelight);
int	my_put_camera_in_list(t_cam **);
int	my_put_object_in_list(t_obj **, t_typeobj);
int	my_put_in_list(t_lexem **, char *, int [2], char *);
int	my_put_params_in_list(t_lexem_params **, char *, char *);

#endif /*!MANAGE_LIST_H_*/
