/*
** lexem.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue May 13 12:10:02 2014
** Last update Sat May 17 17:52:51 2014 
*/

#ifndef LEXEM_H_
# define LEXEM_H_

int	check_line(char *, t_lexem **, int);

#endif /*!LEXEM_H_*/
