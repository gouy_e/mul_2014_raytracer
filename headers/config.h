/*
** config.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 19:58:28 2014
** Last update Fri Jun  6 18:54:10 2014 
*/

#ifndef CONFIG_H_
# define CONFIG_H_

# define CHAR_AUT "abcdefghijklmnopqrstuvxyz0123456789_-"
# define CHAR_TOKEN "\ =\"'"
# define CHAR_VALUE "abcdefghijklmnopqrstuvxyzABCDEF0123456789.-\"/"
# define CHAR_NUMBER "0123456789-."

#endif /*!CONFIG_H_*/
