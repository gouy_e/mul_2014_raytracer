/*
** error.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri May 16 20:27:58 2014
** Last update Sat May 17 19:47:46 2014 
*/

#ifndef ERROR_H_
# define ERROR_H_

int	print_success(char *);
int	print_error(char *, char, int);
int	print_xml_error(char *, int);

#endif /*!ERROR_H_*/
