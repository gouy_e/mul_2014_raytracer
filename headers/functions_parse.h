/*
** functions_parse.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue May 13 12:17:46 2014
** Last update Tue May 13 12:19:08 2014 
*/

#ifndef FUNCTIONS_PARSE_H_
# define FUNCTIONS_PARSE_H_

int	is_valid_char(char, char *);
char	*concaten_strs(char *, char *);

#endif /*!FUNCTIONS_PARSE_H_*/
