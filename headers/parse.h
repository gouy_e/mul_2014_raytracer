/*
** parse.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:50:12 2014
** Last update Thu May  8 18:51:49 2014 
*/

#ifndef PARSE_H_
# define PARSE_H_

int	parse_file(const int, t_general *);

#endif /*!PARSE_H_*/
