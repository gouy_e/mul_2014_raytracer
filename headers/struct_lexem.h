/*
** struct_lexem.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May  8 18:54:11 2014
** Last update Mon Jun  2 19:50:19 2014 
*/

#ifndef STRUCT_LEXEM_H_
# define STRUCT_LEXEM_H_

typedef struct		s_lexem_params
{
  char			*name;
  char			*value;
  struct s_lexem_params	*next;
}			t_lexem_params;

typedef struct		s_lexem
{
  char			*name;
  int			closed;
  int			line;
  char			*value;
  char			*parent;
  t_lexem_params	*params;
  struct s_lexem	*next;
  struct s_lexem	*prev;
}			t_lexem;

#endif /*!STRUCT_GENERAL_H_*/
