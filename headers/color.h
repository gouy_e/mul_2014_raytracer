/*
** color.h for Raytracer in /home/hirt_r/rendu/raytracer
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Fri May 23 14:47:03 2014 Romain Hirt
** Last update Fri May 23 16:03:46 2014 Romain Hirt
*/

#ifndef COLOR_H_
# define COLOR_H_

int	color_lumi(t_color *, double);
int	color_shiny(t_color *, t_color, double);
int	add_color(t_color *, t_color);

#endif /*!COLOR_H_*/
