/*
** rotation.h for Raytracer in /home/hirt_r/rendu/raytracer
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Thu May 29 17:58:49 2014 Romain Hirt
** Last update Thu May 29 17:59:42 2014 Romain Hirt
*/

#ifndef ROTATION_H_
# define ROTATION_H_

t_coord	make_rotation(t_coord, t_coord);

#endif /*!ROTATION_H_*/
