/*
** type_obj.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri May 16 21:21:55 2014
** Last update Fri May 16 21:25:46 2014 
*/

#ifndef TYPE_OBJ_H_
# define TYPE_OBJ_H_

t_typeobj	type_sphere();
t_typeobj	type_cylindre();
t_typeobj	type_cone();
t_typeobj	type_plan();
t_typeobj	type_cubetroue();

#endif /*!TYPE_OBJ_H_*/
