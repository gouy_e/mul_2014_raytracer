/*
** get_next_line.h for get_next_line.h in /home/gouy_e/rendu/CPE_2013_getnextline
**
** Made by gouy_e
** Login   <gouy_e@epitech.net>
**
** Started on  Thu Nov 21 11:40:38 2013 gouy_e
** Last update Thu May  8 18:42:25 2014 
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define READ_SIZE	30
# define BUF_SIZE	500

char			*get_next_line_file(const int fd);

#endif /* !GET_NEXT_LINE_H_ */
