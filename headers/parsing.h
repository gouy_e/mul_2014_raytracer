/*
** parsing.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Thu May 15 13:58:26 2014
** Last update Thu May 15 13:59:21 2014 
*/

#ifndef RAYTRACER_H_
# define RAYTRACER_H_

int	my_parsing(t_general *, t_lexem *);

#endif /*!RAYTRACER_H_*/
