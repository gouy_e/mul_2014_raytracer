/*
** lexem_check.h for raytracer in /home/darnat_a/rendu/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Tue May 13 12:29:43 2014
** Last update Fri Jun  6 19:06:50 2014 
*/

#ifndef LEXEM_CHECK_H_
# define LEXEM_CHECK_H_

char	*get_token_name(char *, int, int);
int	detect_token(char *, t_lexem **, int);
char	*get_name_of_params(char *, int);
int	get_token_params(t_lexem **, char *, int, int);
int	get_token(char *, int);
int	my_set_data(char *, int *, t_lexem **, int);
int	check_close(char *, int, t_lexem **, int);

#endif /*!LEXEM_CHECK_H_*/
