/*
** pixel.h for Raytracer in /home/hirt_r/rendu/raytracer
** 
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
** 
** Started on  Mon May 19 17:10:37 2014 Romain Hirt
** Last update Mon May 19 17:15:24 2014 Romain Hirt
*/

#ifndef PIXEL_H_
# define PIXEL_H_

int	my_aff_image(t_general *g);
void	pixel_img(t_aff *, int, int, t_general *);

#endif /*!PIXEL_H_*/
