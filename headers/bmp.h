/*
** bmp.h for raytracer in /home/darnat_a/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri Jun  6 18:53:10 2014
** Last update Fri Jun  6 18:53:29 2014 
*/

#ifndef			BMP_H_
# define		BMP_H_

typedef unsigned char	BYTE;
typedef unsigned short	WORD;
typedef unsigned int	DWORD;
typedef unsigned int	LONG;

typedef struct		s_bitmap
{
  DWORD			size;
  LONG			width;
  LONG			height;
  WORD			planes;
  WORD			bpp;
  DWORD			compression;
  DWORD			size_of_bitmap;
  LONG			horz_resolution;
  LONG			vert_resolution;
  DWORD			colors_used;
  DWORD			colors_important;
  DWORD			red_mask;
  DWORD			green_mask;
  DWORD			blue_mask;
  DWORD			alpha_mask;
  DWORD			c_s_type;
  LONG			redx;
  LONG			redy;
  LONG			redz;
  LONG			greenx;
  LONG			greeny;
  LONG			greenz;
  LONG			bluex;
  LONG			bluey;
  LONG			bluez;
  DWORD			gammared;
  DWORD			gammagreen;
  DWORD			gammablue;
}			t_bitmap;

int			export_bmp(t_general *);
int			print_success(char *);
int			print_error(char *);

#endif /*!BMP_H_*/
