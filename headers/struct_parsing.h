/*
** struct_parsing.h for raytracer in /home/darnat_a/raytracer
**
** Made by
** Login   <darnat_a@epitech.net>
**
** Started on  Fri Jun  6 18:54:39 2014
** Last update Fri Jun  6 18:54:40 2014 
*/

#ifndef STRUCT_PARSING_H_
# define STRUCT_PARSING_H_

typedef struct	s_parsing	t_parsing;

typedef struct			s_parser
{
  char				*name;
  int				(*ptr)(t_general *, t_lexem **, t_parsing *);
}				t_parser;

typedef struct			s_parse_camera
{
  char				*name;
  int				(*ptr)(t_lexem **, t_cam *);
}				t_parse_camera;

typedef struct			s_parse_object
{
  char				*name;
  int				(*ptr)(t_lexem **, t_obj *);
}				t_parse_object;

typedef struct			s_parse_light
{
  char				*name;
  int				(*ptr)(t_lexem **, t_light *);
}				t_parse_light;

typedef struct			s_parse_raytracer
{
  char				*name;
  int				(*ptr)(t_general *, char *, int);
}				t_parse_raytracer;

typedef struct			s_parse_typelight
{
  char				*name;
  t_typelight			type;
}				t_parse_typelight;

typedef struct			s_parse_typeobj
{
  char				*name;
  t_typeobj			type;
}				t_parse_typeobj;

struct				s_parsing
{
  t_parser			parser[5];
  t_parse_raytracer		raytracer[6];
  t_parse_object		objects[14];
  t_parse_typeobj		objtype[7];
  t_parse_camera		cameras[3];
  t_parse_light			lights[5];
  t_parse_typelight		ligtype[3];
};

#endif /*!STRUCT_PARSING_H_*/
