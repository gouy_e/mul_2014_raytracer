/*
** raytracer.h for Raytracer in /home/hirt_r/rendu/raytracer
**
** Made by Romain Hirt
** Login   <hirt_r@epitech.net>
**
** Started on  Sat May 17 15:06:33 2014 Romain Hirt
** Last update Thu Jun  5 16:47:37 2014 Romain Hirt
*/

#ifndef RAYTRACER_H_
# define RAYTRACER_H_

int	check_limit(t_ray *, double, t_obj *);
int	raytracer(t_general *);
t_coord sub_vector(t_coord, t_coord);
t_coord add_vector(t_coord, t_coord);
t_coord cross_vector(t_coord, t_coord);
double	angle_vector(t_coord, t_coord);
double	resolve_eq(t_coord);
double	resolve_eq_sec(t_coord);
t_coord	calc_inter_pt(t_ray *, double);
int	get_lumi(t_aff *, t_general *);
int	get_pixinfo(t_aff *, t_general *, t_ray *, int);
t_coord	mult_vector(t_coord, double);
double	scal_vector(t_coord, t_coord);
double	norm(t_coord);
int	make_reflection(t_aff *, t_general *, t_ray *, t_obj *);
int	make_transp(t_aff *, t_general *, t_ray *, t_obj *);
double	resolve_quad(t_equ);
void	*ray_cast(void *);

#endif /*!RAYTRACER_H_*/
