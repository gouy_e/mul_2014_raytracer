##
## Makefile for Makefile in /home/gouy_e/rendu/PSU_2013_my_ls
##
## Made by gouy_e
## Login   <gouy_e@epitech.net>
##
## Started on  Fri Nov 22 17:44:04 2013 gouy_e
## Last update Fri Jun  6 19:25:29 2014 
##

NAME	= rt

SRCS	= sources/main.c \
	  sources/bmp.c \
	  sources/thread.c \
	  sources/parser/parse_raytracer.c \
	  sources/parser/parse_objects.c \
	  sources/parser/parse_cameras.c \
	  sources/parser/parse_lights.c \
	  sources/parser/raytracer_functions.c \
	  sources/parser/objects_functions.c \
	  sources/parser/objects_functions_part2.c \
	  sources/parser/objects_functions_part3.c \
	  sources/parser/lights_functions.c \
	  sources/parser/xml_error.c \
	  sources/parser/parse.c \
	  sources/parser/parsing.c \
	  sources/parser/init_func_tab.c \
	  sources/parser/init_func_tab_part2.c \
	  sources/parser/lexem.c \
	  sources/parser/lexem_check.c \
	  sources/parser/lexem_get.c \
	  sources/parser/functions.c \
	  sources/parser/manage_list.c \
	  sources/parser/manage_list_part2.c \
	  sources/parser/get_next_line_file.c \
	  sources/objects/sphere.c \
	  sources/objects/plan.c \
	  sources/objects/cone.c \
	  sources/objects/tore.c \
	  sources/objects/cubetroue.c \
	  sources/objects/cylindre.c \
	  sources/rayfunc.c \
	  sources/pixel.c \
	  sources/color.c \
	  sources/solver.c \
	  sources/light.c \
	  sources/rotation.c \
	  sources/raytracer.c

CFLAGS	= -W -Wextra -Werror -Wall -ansi -pedantic -O3

CFLAGS	+= -I headers/

LDFLAGS = -lmy -Llib_my -lm -Lminilibx -lmlx_$(HOSTTYPE) -L/usr/lib64/X11 -lXext -lX11 -lpthread

OBJS	= $(SRCS:.c=.o)

CC	= gcc

RM	= rm -f

$(NAME): $(OBJS)
	@ if [ ! -e "lib_my/libmy.a" ]; then \
		cd lib_my && make && make clean; \
	fi
	$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) $(CFLAGS)

all: $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all
